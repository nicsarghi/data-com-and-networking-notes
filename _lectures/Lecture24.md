---
layout: post
title: Lecture 24
date: 2022/12/07
file: DataComm-Lecture24.pdf
dir: /lectures/
--- 

# Revision
## MQTT
- Publish-Subcribe model
- Uses a broker (server) that the clients connect to 
- A topic serves to be an endpoint where data is transmitted
- The broker maintains an internal table that links the publishers and subscribers to a topic.
- When you publish, there is an initial transfer to the broker. Then, another transfer is made to the client (acts as a response). Any clients subscribed to the topic will receive data from the broker. The client then sends an awknowledgement to the server.
- Questions on the final will mainly coverr subjects we **covered in the project** there will be theory, but topics such as QoS won't be as strictly graded. Revise the labs and the projects that you've worked on.

## ASCII encoding and hex/binary
- Remember how to read hexadecimal values, because you may be provided an ASCII table in the exam
- Make sure to know the difference between ASCII and UTF+16, try to read examples from the slides on how strings are encoded. 
- Know difference between Big Endian and Little Endian possibly

## Question Types
- MC: Multiple choice questions, likely theory dense and requires you to memorize concepts
- Explanation questions: Consists of the key points in the theory.
  - Worry about circuit-switching and packet-switching. Know the difference between them. Packet switching is used more commonly nowadays. 
  - In circuit switched networks, a single channel is established for a connection and is usually a telephone line. Telephone lines CAN be used for internet, but it's slower. Modems previously connected to the ISP through the phone lines to get internet. 
  - In packet switching, most connections use fiber optics, coaxial cables & copper wiles as they are generally more efficient ways of transmitting data. 
  - Know how routers work: packets are transmitted by passing them from router to router.
- Code -> Don't memorize details, you might need to know some JavaFX. Know what you are coding, and make sure to study the labs and read the examples.
- ECC and Encryption: More likely to be explanation but its possible that some simple multiple choice questions also cover them.

## Security Key concepts
- Symmetric and asymmetric key encryption
- Digital certificate -> Know how they work and how public keys are used here
- Hashing 
- Certificate authority
- Know what algorithms exist: Sha-256 and Elliptic-Curve. Keep track of the more modern ones (ie state of the art algorithms most appropriate for encryption). 
- The reason why we want to be aware of newer key algorithms is because older ones have been cracked and proven to be vulnerable to attacks.
- AES is a common algorithm.
- See the video on new encryption algorithms.
- Feel free to check the complementary material on Moodle, because it will help you understand these concepts better. 

## Transmitting data from long distances 
- Cellular towers
- WiMax
- Fiber optic cables (Some are placed at the bottom of seabeds!)
