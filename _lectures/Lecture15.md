---
layout: post
title: Lecture 15
date: 2022/11/03
file: DataComm-Lecture15.pdf
dir: /lectures/
--- 
# Connecting the PI and the Breadboard
- The board circuits are NOT hot plug, remember to shut off the pi when you configure the circuits.
- Remember to make sure you're connecting the ribbon cable in the right orientation. Check the Freenove guide if you have to. If the pins are incorrectly placed, you might risk burning one of them.

## Groups
- If you don't have a team already, Ponce will assign one to you. He'll also assign someone to your team if a person is missing.

## Java multithreading and testing
- As said previous, Google is helpful here.
- But also make sure to check the console. System.out.println is your best friend!

## Ponce's Java Configuration
- Ponce posted his java configuration for the java runner on Moodle (for aliases and exports)
    ```
    #This is my rc file (home directory) NOTE: I use macOS

    export JAVA_8_HOME=$(/usr/libexec/java_home -v1.8)
    export JAVA_11_HOME=$(/usr/libexec/java_home -v11)
    export JAVA_17_HOME=$(/usr/libexec/java_home -v17)

    alias java8='export JAVA_HOME=$JAVA_8_HOME'
    alias java11='export JAVA_HOME=$JAVA_11_HOME'
    alias java17='export JAVA_HOME=$JAVA_17_HOME'

    # default to Java 17
    java17
    ```
- Contains the JAVA 8, 11 and 17 JREs. It also uses alises for those exports. 
- Defaults to java 17
- **The directories are adjusted to Ponce's computer**, so make sure to adjust them to yours. This means that you need to replace ``/usr/libexec/java_home`` with the correct path to where your java_home directory is.

## Design Patterns
- For designing the UML, make sure to think about which design patterns to use for the project
- It's good practice and necessary for the proper execution of the project :)
- [Refactoring.guru](https://refactoring.guru/) is a good site for brushing up on your design pattern knowledge
- You can also [Download the original pdf version of the Design Patterns book](https://github.com/mkejeiri/Java-Design-Pattern/blob/master/Design%20Patterns%2C%20Elements%20of%20Reusable%20Object-Oriented%20Software.pdf)
