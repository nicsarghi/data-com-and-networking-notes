---
layout: post
title: Lecture 19
date: 2022/11/17
file: DataComm-Lecture19.pdf
dir: /lectures/
--- 

# Error Correction
- For a receiver to correct the error with no
further help from the transmitter requires a
large amount of redundant information to
accompany the original data
  - This redundant information allows the receiver
to determine the error and make corrections

## Hamming Codes
- This type of error control is often called
forward error correction and involves codes
called Hamming codes
- Hamming codes add additional check bits
to a character
  - These check bits perform parity checks on
various bits

## Error correction video (Hamming Codes)
[See the video here](https://www.youtube.com/watch?v=X8jsijhllIA&feature=youtu.be)

# Shielding 
"A shielded cable or screened cable is an electrical cable that has a common conductive layer around its conductors for electromagnetic shielding. This shield is usually covered by an outermost layer of the cable."
- The point of shields are to protect the electrical cables from **electromagnetic radiation and electric fields**. This is because this can produce noice and corrupt the data.

## Types of shielding
- F/UTP: FOILED WITH UNSHIELDED TWISTED PAIRS.
- S/UTP: SHIELDED WITH UNSHIELDED TWISTED PAIRS

Generally, there's a mesh or a magnetic cover to avoid electromagnetic induction.  
**NOTE:** Fiber optic transmissions are being used more commonly and are better because of this reason.

## Reminder on Parity:
### Simple Parity and Longitudinal parity
    - Checks for Even/Odd bits, Longitudinal does a matrix
### Arithmetic
    Sums of values together (ie a+b)
### Cyclic redundancy
    Uses polynomials: ax^3+bx^2+cx

## Reminder: Cyclic Redundancy check
- ECC code: finds changes in data, adds code to the end of the message
- If the codes check out, then the data is considered accurate.
- CRC codes usually check for errors in the errors, and can detect 99% of most data errors, and are generally simple to implement
  - Works on the data link later
  - Shift feedback register

## CRC video
[See the video here](https://www.youtube.com/watch?v=iwj8ZgyzqZk&feature=youtu.be)

## Receiving an erroneous code
Two strategies:
- Resend the data
- Try to correct the data

