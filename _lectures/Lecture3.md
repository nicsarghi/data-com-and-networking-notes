---
layout: post
title: Lecture 3
date: 2022/09/1
file: DataComm-Lecture3.pdf
dir: /lectures/
---  

# Lab Commands Discussion
- Two linux interfaces exist: wlan0 and eth0
- There's a specific naming scheme for identifying them.
- The w at the beginning is usually used to indicate interfaces that wireless connections
- The e is used to indicate network interfaces that are ethernet connections. Wlan refers to a wireless connection to the LAN ([More on that here](https://rakeshjain-devops.medium.com/network-interface-naming-3183bcd27389))
- Important to ensure security by encrypting your password and getting a hash, since leaving the password directly there can make it easy for malicious parties to steal login information.

## MAC
- For every NIC, you will have a physical address for it (the MAC address). 
- It's physically embedded in the circuit.
- In the OSI model, it can be included in the Data Link layer.
- Switches also operate on the second layer.
- Switches assign MAC addresses and store them in a table that associates it with an IP. This table is called the ARP table.
- In the MAC Address table, a MAC address is associated to an ethernet interface.
- Network switches build MAC address tables with entries comprised of destination MAC address, port and VLAN membership  ([More here](https://community.cisco.com/t5/networking-knowledge-base/network-tables-mac-routing-arp/ta-p/4184148))

## Subnet mask
- The mask shows what portion of the IP consists of the host and the network
- Example: IP is 196.128.3.22 and the mask is 255.255.255.0
- The network is 196.128.3 and the host is 22

## Default Gateway
- Default node that connects to the LAN, allows you to send information to an external network. It's the entry point of every packet sent externally.
- Usually it's the router that connects to other remote networks.
- Then point is to pass information outside the network when the desination isn't known (aka IPs that are not in the LAN)

## Ping 
- Pinging google.ca will make them look up the domain by sending a request to a DNS server and requiring the address
- Uses ICMP to send the ping (ICMP echo request, and then expects an ICMP echo reply)
- Used to test connectivity between devices

## TTL
- Time to live indicates the amount of hops it takes for the packet to be discarded
- The point is to limit the lifespan of a packet traversing through a network so that it doesn't stay there indefinitely. 
- This is to avoid congestion, especially in cases where the desination packet is far, far away.

## Traceroute
- Functions by sending ICMP echo packets with variable TTL values. It reports back the IP addresses of all routers that were pinged in between. 
- The point is to track the hops taken by a packet on an IP network. 
- Traceroute also records the time taken for each hop the packet makes during its route to the destination.

## ARP (Address Resolution Protocol)
- ARP request is sent from a host (desktop), to learn the MAC address of a destination server after DNS has already resolved destination server IP address.
- Determines the physical address that's associated with an IP. 
- Operates between data link and IP.
- It is only Layer 3 network devices (routers, Layer 3 switches, firewalls) and hosts that create ARP tables. Layer 2 switches do not create an ARP table.
- Router has an IP table and a MAC port table  
![ARP table example](images/arp-table.png)

## Network Access Layer ([from Cisco](https://www.cisco.com/E-Learning/bulk/public/tac/cim/cib/using_cisco_ios_software/linked/tcpip.htm#xtocid291426))
- The network access layer is the lowest layer in the Internet reference model. This layer contains the protocols that the computer uses to deliver data to the other computers and devices that are attached to the network. 
- They define how to use the network to transmit a frame, which is the data unit passed across the physical connection.
- They exchange data between the computer and the physical network.
- They deliver data between two devices on the same network that are identified by a NIC.