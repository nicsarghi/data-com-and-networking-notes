---
layout: post
title: Lecture 25
date: 2022/12/08
file: DataComm-Lecture25.pdf
dir: /lectures/
--- 

### Credit to Kelsey for taking these notes

# Final exam topics
## Project / Coding questions

**MQTT**

- Publish-Subscribers
- Broker

**GPIO**

- Sensors
- Activators
- Data communication

**TLS**

- Security
- One-to-one
- peer-to-peer
- Keystore
- Encryption

## Format

- Multiple Choice -> 25-35
- Short Answer -> 10
- Coding -> 3-4

## Additional Notes
- He said not to memorize to much detail with the tables
- Synchronous interface is not on the test cause its not on the project
- Content from the project is also present in the coding questions


