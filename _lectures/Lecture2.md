---
layout: post
title: Lecture 2
date: 2022/08/31
file: DataComm-Lecture2.pdf
dir: /lectures/
---  

# Some reminders
- The point of these lectures is to strengthen your knowledge from your last semesters. Mainly they're reminders of what you lerned in Infrastructure I and Linux II.
- Ponce mentioned to always consult the slides, especially when revising. All material explained below **can be found in the slides**, but this will still provide additional explanations.

# History and evolution of communication
## Evolution of communication
- *Before, most communication was done over Snail Mail*. If you wanted to send a letter across the ocean, you'd have to wait a really, really long time. 
- It was only until the proper technology (phones, telegrams) were we able to communicate over long distance
- Eventually, when we figured out how to use transistors to store and transmit data digitally, were we able to really expand communications.
- Which then leads us to the **internet and the IP protocol**, which is one of the protocols that allows us to send data to other computers (and thus other people)

## In the beginnining
- **The first communication was between two computers**: the University of California, Los Angeles (UCLA) and the Stanford Research Institute on October 29, 1969.
- They then started to expand the network by opening up connections between more universities. Over time, this expanded into a 4 node network, and, over time, other networks would emerge. These networks would eventually become merged over time and it would **slowly transform into the Internet**. 
- It was around the late 80s to mid 90s that Internet Service Providers had made network access available to commercial customers.

# Switching
## Circuit Switching 
- Old (1920), and has more infrastructure compared to the internet (established in the 60s, more developed in 80-90s)
- Reason is because it's what Plain old telephone service (POTS) uses 
- Can still be used for internet. ADSL modem, Dial up internet from back in the day.
- First approach to connecting two nodes, which consists of one singular path (circuit) that will last the conversation
- Node A is connected to a network, and it wants to connect to node B. The network has a bunch of devices in between
- The nodes establish a circuit (path) in between Node A and B (used in telephone communication). Done if you wanted to do a call. If a node C wanted to estqablish a communication, it could not use a node from the circuit A-B
- PSTN: **Public Switched Telephone Network**
- POTS: **Plain Old Telephone Service**
![Circuit Switched Network](images/circuit_switching.png)

## Packet Switching
- Used by TCP/IP networks. Also the Internet
- If Node A wanted to send data to Node B (with a network inbetween), the data is **fragmented into smaller, manageable-size chunks (Packets)** and sent along the network. 
- Each **packet contains the address** so that it can be sent to a recipient
- The network will find a trajectory to Node B. Each packet can take different paths to the same destination. 
- When they arrive at the destination, they get **reassembled** to form data
- The packet sizes can differ from software to software, as they may determine that size (even if they use the same protocol).
![Packet Switched Network](images/packet-switching.png)

# Protocols and OSI

## Protocols
- Described as the "rules of communication". Protocols define **how connected devices are able to communicate**
- AKA It is a set of rules that defines how two nodes communicate
- When multiple protocols are grouped together, this forms a **protocol suite** or a protocol family.
- Implementation of protocol suite AKA a protocol stack: software implementation that allows multiple protocols to work together.
- TCP/IP protocol (not new, still used now widely), consists of 4 layers

## OSI Reference Model
- The point is to have a "divide and conquer" approach to tackling issues regarding network communications. It sort of serves to answer for the various problems that might occur during network communication. From the powerpoints:
```
For network communications to take place, hundreds of questions must be answered by a set of protocols.
Evaluating and working with these hundreds of questions would be unmanageable.
```
- *Dont memorize the dates/history but they might be good to know*
- In 1977, ISO (International Organization for Standardization) adopts Open Standards Interconnection (OSI) model
- Standardised way to divide communications (encapsulate them) into layers so that it resolves each question (given problem) step by step. Basically, it sort of splits these issues into 7, much more manageable groups that handle network communication in different ways.
- AKA 7 layers of communication
- Note that it's a standard, and the referense model for networking
- Companies will use this reference model when developing their technology
- They separate the networking/programming into layers by designing APIs for each layer

![OSI layers](images/osi-layers.png)

## Example OSI interaction
- A protocol serves to solve a problem (or problems) posed by one of the seven layers in the OSI model. AKA a protocol can address a specific layer 
- A *protocol suite* can solve for all of these problems, 
![PTP communication layer dissection](images/ptp-protocol.png)

## Wireless communications (Physical Layer)
- Important to know
- Before, data was transmitted through cables/wires
- We now use wireless communication to send data
- Air is used to transmit data w/ the IPS
- Consits of a lower layer as it's the physical way of transmitting information

## OSI Layers
- **Remember and know what these layers are.**
- You don't need to remember what all these protocols are, but it's important to know which of the most commonly used ones go where. 

### Application (Layer 7)
- Common Protocols: BitTorrent, DNC, DSNP, **DHCP,  FTP, HTTP(S)**, IMAP, MIME, NTP, POP3, RADIUS, RDP, SMTP
- Devices: Gateway, Firewall, Endpoint device (server, PC, mobile device, etc.) 

### Presentation (Layer 6)
- Common Protocols: SSL, TLS
- Devices: Gatewa, Firewall, server, PC

### Session (Layer 5)
- Common Protocols: L2F, L2TP, NetBIOS, NFS, RPC, SMB, **SSH**
- Devices: Gatewa, Firewall, server, PC

### Transport (Layer 4)
- Common Protocols: AH (over IP/IPSec), BGP, ESP (over IP/IPSec), **TCP, UDP**
- Devices: Gatewa, Firewall, server, PC

### Network (Layer 3)
- Common Protocols: ICMP, IGMP, IGRP, IPv4, IPv6, IPSec, GRE, OSPF, RIP
- Devices: Router, brouter, Layer 3 switch

### Data-Link (Layer 2)
- Common Protocols: ARP, Ethernet (IEEE 802.3), FDDI, Frame Relay, IND, L2TP, PPP, MAC, NPD, RARP, STP, Token Ring, VLAN, Wi-Fi (IEEE 802.11), WiMax (IEEE 802.16), X.25
- Devices: Bridge, modem, network card, Layer 2 switch

### Physical (Layer 1)
- Exists to ensure that the hardware requirements of establishing and maintaining a network are met so that data can be transmitted successfully.
- Meant to translate information from Layer 2 into electromagnetic signals and sends them over a physical medium. Signals might be digital or analog signals.
- Common Protocols: Bluetooth, DSL, Ethernet (Physical Layer), USB, Wi-Fi (Physical Layer)
- Devices: Hub, repeater, cable, fiber, wireless
- Consists of electrical signals that are sent through physical wires. Sometimes ISPs use sensors to detect traffic

## TCP/IP model
- MAIN protocol suite that nodes in a networks use to communicate. **Allows nodes to transmit and receive data on the Internet**
- In packet switched networks, this is usually how computers communicate (by sending and receiving data)
- *Developed by Robert Kahn and Vinton Cerf as the next generation of ARPANET*
- *Development began in 1974. In 1982, it started to be adopted into networks. By 1989 was in public domain*
- You can simplify the OSI to create the TCP/IP model:
![TCP-IP and OSI Model](images/osi-to-tcpip-protocol.png)

### Consists of
4. Application Layer: Applications and Processes using the network
3. Host-to-Host Transport Layer: End-to-end data delivery services
2. Internet Layer: Datagram and handling how data is routed 
1. Network Access Layer: Routines for accessing physical networks

# Network Topology
- Topology: The way in which two computers are connected. Consists of a layout of *how* they are connected.
- More specifically: A map of the network that shows how devices connect to one another and how they use a connection medium to communicate
- **A node (server, computer, smartphone, etc.) can physically connect to the network and has an assigned IP host address**
- Example, Point to point refers to two computers only
- There's also star connections, mesh-like, tree-like, etc.

## Physical Topology
- Describes the actual network devices, and how they're physically connected to the network.
- This can consist of how the wires can connect the node physically to the network, or the wireless connections inside of it.  
![Logical Topology graph](images/physical-topology.png)

## Logical Network Topology
- How the Network *logically* works.
- Describes how data is transferred through the network.
- Focuses more on the technology that operates in the network. 
- EX: You can simulate a network (and determine the topology) through virtual networking  
![Logical Topology graph](images/logical-topology.png)

## Bus Topology
- Looks like what its name says (like a bus)
- Basically every node is connected to a main cable, which is the backbone of the network
- Only one device can communicate at a time. Collissions are an issue here because 2 devices may try to transmit data at the same time.
- Network is dead if the cable breaks (since its the backbone of the network)
- Bandwith is an issue
- **Obsolete, nobody uses this because of the aforementioned issues**. It's better to use the star topology with a switch here  
![Bus Topology](images/bus-topology.png)

## Ring Topology
- Nodes are connected in a circular pattern. Literally connected to eachother in a chain that looks like a ring
- A Token ring consists of a network that sends data to the next computer if permission is granted. Permission is usually granted via a token that is circulated around the network.
- If the outer ring breaks (aka one of the nodes are down or one of the connections are down), then the network is dead. One solution is to have an inner ring as a backup.
- Bandwith is also an issue here.
- **Obsolete because of the ring breaking issue.**  
  
![Ring Topology](images/ring-topology.png)  

## Hierarchical Topology
- A parent connects to a child node, and the child node can have an arbitrary amount of other child nodes. 
- Parent nodes end up having more bandwith
- If a parent node breaks, then the connections between its children is gone (The only connections are the local networks formed by the child nodes and its own children)

![Hierarchical Topology](images/hierarchical-topology.png)

## Star Topology
- Refers to nodes connected to a main connection point (can be a swtich, hub or router)
- Can be extended with repeaters
- Resembles an asterisk (*)
- If the link between two networks gets broken off, then they cannot communicate (or if one of the hubs connecting between them is down)
- Cheaper cost, requires less infrastructure (you can simply establish multiple connections to a single switch)
- Obviously if the central connection point fails, then the network is dead  
![Star topology](https://cdn.comparitech.com/wp-content/uploads/2018/11/star-Topology.jpg)

## Star/extended star
- Refers to two stars connected to eachother  
![Star extended](https://www.conceptdraw.com/How-To-Guide/picture/star-network-topology-diagram.png)

## Mesh
- Every node is connected to eachother
- *Or, minimally they're interconnected as much as possible*
- Messages sent can take any of the possible paths from source to destination
- If a connection fails, then there is another route in the network
- More expensive, but communication is reliable. Which is why most people use partial mesh networks
- What the Internet is based off of (backbone of the Internet)  
![Mesh topology](https://1.bp.blogspot.com/-wLsfGFV1U_s/Xa3_aOv5tyI/AAAAAAAABRU/-ZDGq8ZJKgwaumhkN-SMLYRVoQ4czlOjQCLcBGAsYHQ/s1600/what-is-mesh-topology.jpg)

# IP-Based Communications

## TCP (Transmission Control Protocol)
- The point is to guarantee the delivery of a **reliable stream of data** between two programs
- One of the most popular protocols that organizations use to communicate on the Internet
- OSI Layer 4

## IP (Internet Protocol)
- Makes it possible to deliver packets across a complex network to a destination (Routing)
- Handles the routing decisions necessary to get packets from their source to the destination (IP addresses)
- OSI Layer 3

## Applications and Communications using IP
- Most applications are designed with TCP/IP in mind, since its the most appropriate transport protocol
- Because it's so prevalent, network presence is expressed as an IP address (ie you're on the network if you have an IP)
- IP addresses identifies devices as nodes on a network
- Private IP addresses can also identify nodes in an organization
- Applications only need to reference the IP address

## Internetworking 
- Describes connecting LANs together in a WAN
- The LANS then communicate using protocols
- Network devices interoperate at different layers of the protocol stack

## Internetworking on the Data Link Layer
- Internetworking at the Data Link (aka using MAC Addresses) can include MAC Layer Bridging
- This is usually handled during switching.
- MAC bridging is a mechanism that allows devices on a LAN to have a direct connection to the WAN (https://arris.secure.force.com/consumers/articles/General_FAQs/SBG8300-MAC-Bridging-Setup/?l=en_US&fs=RelatedArticle)  
![MAC Bridging](images/datalink-layer-internetworking.png)

## Internetworking on the Network (IP) Layer
- Internetworking on the IP layer consists of Network Layer Routing AKA IP Routing.
- It's a set of protocols that determines the path a packet has to follow so that it can travel across multiple networks and reach a final destination
- For example, routers can use a forwarding table that correlates final destinations with the next hop address (https://www.metaswitch.com/knowledge-center/reference/what-is-ip-routing)  
![Network Layer Routing](images/network-layer-osi-internetworking.png)

## Client/server architecture
- When a user interacts with an app (ie through software that runs locally), this is usually considered the client. 
- The client connects to a network, and then requests something from a server, which is usually stored on an external network (or within the same network). Generally, the server is just a device that will provide or "serve" data to the client. 


## Peer-to-peer internetworking
- Uses peers (connections between two devices) to exchange messages without depending on a central server that will handle connections or messages.
- In a P2P network, the computers are linked together and share equal permissions and responsibilities

### Most of today’s digital networks can support client/server and peer-to-peer communications

## Internet
- Many nodes connected together, consists a huge network

## Intranet
- Internal network that members of an organization can only access
- Private network with the organization's IP network infrastructure
- A firewall is usually used to protect the intranet from unwanted packets that originate from the extranet.  
![Illustration of a extranet and intranet](images/intranet-extranet-protection.png)