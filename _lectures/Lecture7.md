---
layout: post
title: Lecture 7
date: 2022/09/14
file: DataComm-Lecture7.pdf
dir: /lectures/
---  
# Raspberry Pi + Brief project explanation

## Make sure to setup the environent for java, mainly for the next lab.

## The project
- MQTT protocol with sockets
- It's going to send data through sockets
- Done in teams of 3. 
- Ponce described it as having to generate data using the sensors provided by the Freenove kit (he specified a motion sensor in his example, so it may be that.)

## Freenove guide
- [Clone the github repo](https://github.com/Freenove/Freenove_Ultimate_Starter_Kit_for_Raspberry_Pi) for the freenove raspberry pi kit and read the PDF inside of it. You can view the code examples inside as well to familiarize yourself with how the GPIO pins can be manipulated.
- You can also view the freenove kit pdf [through this link](https://moodle.dawsoncollege.qc.ca/pluginfile.php/1389285/mod_resource/content/1/Tutorial.pdf)
- [pinout.xyz](https://pinout.xyz/#) is a good resource for knowing what the pinout is on a raspberry pi. Consult this whenever using the WiringPi to know which pins to use and why they're used.
- [This article details how interacting with SPI devices may work with the pi](https://radiostud.io/understanding-spi-in-raspberry-pi/)
- [This article gives you examples of what projects you might want to make with a pi](https://www.seeedstudio.com/blog/2020/06/03/learn-all-about-the-raspberry-pi/)

# Raspberry Pi 4B Components
![Layout illustrating the components of a pi 4B](images/pi-4b-components.png)
- There are 40 GPIO (General Purpose Input/Output) pins on the raspberry pi, with each labelled with their own types.
- GPIO is the interface for connecting electrical components to the pi.

## GPIO Pins
- Referenced by the following 3 ways:
    - GPIO numbering (Pin numbers assigned by BCM)
    - Physical numbering (Counting across and down)
    - WiringPi GPIO numbering (reference used by WiringPi, see pinout.xyz)
![GPIO pinout](images/gpio-pinout.png)
- 5V indicates a 5 voltage power supply.
- GND (aka ground) is where you connect the end of the circuit.
- Be careful about the voltage going to GPIO, you might accidentally burn the pins them if you're not careful (Remember that strong currents of electricity will heat up the wires and risk burning out the components inside of it.)

# Interfaces
## SPI (Serial Peripherical Interface)
- A synchronous serial bus is used to send data between two micro controllers and small external peripherical devices
- **Peripheral device:** Auxiliary device that is used to put information into and get information out of a computer. Monitors, mice, keyboards, etc. are peripheral devices. 
- **Microcontroller:** A small computer on a single integrated circuit chip. It contains at least one CPU, memory and programmable input/output peripherals.
- It's used for connecting to another circuit with its own syste. Supports 2 chip select lines to interface with 2 SPI slave devices (GPIO 8 and GPIO 7)
- There's a clock, aka the sparate clock or SCK. 
- It also uses Data lines (MISO, MOSI) and a select line (SS) to choose among slave devices. 
- So for example, if you want to connect an SPI slave to the SPI master, you're going to need to connect each pin accordingly. (MOSI to MOSI, MISO to MISO, SCK to SCK...)
![SPI slaves](https://radiostud.io/wp-content/uploads/2017/03/spi.png)

## Inter Integrated Circuit aka I2C
- The I2C interface allows for multiple slave integrated circuits to communicate with one or more master.
- Two directional lines: Serial Data Line (SDA) and Serial Clock Line (SCL)

## I2C vs SPI
- SPI connections need 4 pins (MISO, MOSI, SS and SCK) vs two for I2C (SCL and SDA) to connect any number of slaves
- SPI -> Good for high rate full-duplex connections

## Breadboard
- The positive and negative columns are connected by a single copper strip at the bottom. This means that if you 
- The internal rows are connected by copper strips.
- The copper strips are called power rails. The power rails on the sides of the bread board (pos and neg. columns) are one way you can feed power into an electrical component. You can use jumper wires to connect rows to eachother as well.
- Allows you to create a circuit without soldering anything together. Your lungs live another day.
![Breadboard](images/breadboard.png)
- To connect the breadboard to the raspberry pi, use the GPIO extension board. Make sure it properly corresponds to the RPI's pinout (aka properly placing it). View the [Freenove kit PDF for proper installation instructions.](https://moodle.dawsoncollege.qc.ca/pluginfile.php/1389285/mod_resource/content/1/Tutorial.pdf) 

# Accessories
## LED
- an LED is a type of diode that emits light.
- A diode is defined by it's ability to block a current based on the direction. If a unidirectional current that flows from one end of the LED to another in the correct manner it lights up. Usually, the longer leg of the LED has to be connected to the positive end (the power source.)
- **Diodes only works if the voltage of the positive electrode is higher than that of the negative** When its facing the unintended direction, the resistance of the diode is (in theory) infinite.
- **Remember that V = IR**

## Resistor
- Remember science class? I sure hope you do. Resistors exist... to create resistance. They regulate flow of the current (so you don't burn your pins)
- Resistance is measured in Ohms. You can identify the resistance of a resistor by looking at the colors. The resistance on the freenove kit is written on the labelling of the resistor "pack". Make sure to not lose them!
- Resistors aren't diodes, so it doesn't really matter what direction of the current that it's feeding into. 

## ADC module
- Analog to Digital converter 
- Title says it all, basically this chip converts analog to digital signals. 
- It converts a voltage to a sequence of binary digits.

## Potentiometer
- Resistive element with 3 terminal parts
- Resistance value can be adjusted, movable contact brush that manipulates the resistive substance. 
- Basically, it's a resistor but you can physically adjust it.

## Rotary Potentiometer
- Instead of sliding you can rotate it to adjust the potentiometer.
- You know... Like the volume on a car.

## Thermistor
- Temperature-sensitive resistor
- When the temp changes, the resistor changers
- Used to detect temperature intensity
