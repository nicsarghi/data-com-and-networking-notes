---
layout: post
title: Lecture 22
date: 2022/11/28
file: DataComm-Lecture22.pdf
dir: /lectures/
--- 
# Wireless Networks
- Trasmitted through Air, Vacuum, water, rocks, etc. 
- Wifi, NFC, etc.

## Key Concepts
- Terrestrial microwave transmission
- Satellite microwave transmission
- WiMax broadband wireless systems
- Cellular networks
- Wireless Local Area Networks
- Bluetooth
- Near-Field Communications
- ZigBee
- WiFi security

## Wired Networks vs Wireless
- Mobility
- Cost
- Scalability

## Wireless Media
- Two primary types of wireless network
connections in use today: cellular and Wi-Fi
- Radio, satellite transmissions, and infrared
light are all different forms of
electromagnetic waves that are used to
transmit data
- Technically speaking – in wireless
transmissions, space is the medium
- Can depend on the wireless frequencies that youre using. It's really just transmitted electrons in space. You're modulating the electromagnetic frequencies in the air.
- Long-way radio for ships and military bases, AM/FM for cars, etc...

# Terrestrial Microwave Transmission
- Usually in far away, secluded areas. Like rural towns. We usually set up antennas on a radio tower.
- Land-based, line-of-sight transmission
- Approximately 20-30 miles between towers
- Transmits data at hundreds of millions of
bits per second
- Signals will not pass-through solid objects
- Popular with telephone companies and
business to business transmissions

# Satellite Microwave Transmission
- Similar to terrestrial microwave except the
signal travels from a ground station on earth
to a satellite and back to another ground
station
- Can also transmit signals from one satellite
to another
- Satellites can be classified by how far out
into orbit each one is (LEO, MEO, GEO, and
HEO)

## Orbit Types
- LEO (Low-Earth-Orbit) – 100 to 1000 miles out
  - Used for wireless e-mail, special mobile telephones,
pagers, spying, videoconferencing
- MEO (Middle-Earth-Orbit) – 1000 to 22,300 miles
  - Used for GPS (global positioning systems) and
government
- GEO (Geosynchronous-Earth-Orbit) – 22,300
miles
  - Always over the same position on earth (and always over
the equator)
  - Used for weather, television, government operations
- HEO (Highly Elliptical Earth orbit) –
satellite follows an elliptical orbit
  - Used by the military for spying and by scientific
organizations for photographing celestial bodies  
<br>
![Satelleite Microwave Transmission](images/microwave_transmission.png)

## Configurations
- Satellite microwave can also be classified by
its configuration (see next figure):
  - Bulk carrier configuration
  - Multiplexed configuration
  - Single-user earth station configuration

## Note
- Notice how mechanisms for transmitting signals wirelessly tend to be similar. Router that accept wi-fi connections usually consists of an access point with an antennae to connect to other devices

# WiMax
- Delivers Internet services into homes,
businesses and mobile devices
- Designed to bypass the local loop telephone line
- Transmits voice, data, and video over high
frequency radio signals
- Maximum range of 20-30 miles
- Theoretical speeds of 128 Mbps
- IEEE 802.16 set of standards

# Cellular Telephones
- Wireless telephone service, also called
mobile telephone, cell phone, and PCS
(Personal Communication Systems)
- To support multiple users in a metropolitan
area (market), the market is broken into
cells
- Each cell has its own transmission tower
and set of assignable channels  

<br>

![Cell towers](images/cell-towers.png)

## Calling on a cellphone
- When you enter a phone number on your cell phone and
press send, your cell phone contacts the nearest cell tower and a sets up a channel between it.
- Your mobile identification information is exchanged
to make sure you are a currently participating subscriber
- If you are currently subscribing (aka you are in a CALL), you are dynamically assigned two channels: one for talking, and one for listening; the telephone call is placed; you tal

## Receiving a call on a cell phone
- Whenever a cell phone is on, it “pings” the
nearest cell tower every several seconds,
exchanging mobile ID information; this way, the
cell phone system knows where each cell phone
is
- When someone calls your cell phone number,
since the cell phone system knows what cell you
are in, the tower “calls” your cell phone

# 1st Generation
- AMPS (Advanced Mobile Phone Service) – first
popular cell phone service; used analog signals
and dynamically assigned channels
- D-AMPS (Digital AMPS) – applied digital
multiplexing techniques on top of AMPS analog
channels

# 2nd Generation
- PCS (Personal Communication Systems) –
essentially all-digital cell phone service
- PCS phones came in the following technologies:
  - TDMA – Time Division Multiple Access
  - CDMA – Code Division Multiple Access
  - GSM – Global System for Mobile Communications
  - GPRS (General Packet Radio Service) used in GSM
  networks

# 3rd Generation
- UMTS (Universal Mobile Telecommunications
System) – also called Wideband CDMA
  - The 3G version of GPRS
  - UMTS not backward compatible with GSM (thus
requires phones with multiple decoders)
- Whereas the first 2 generations of cellular
telephones supported voice then text, 3G defined
transition to broadband access

# 4th Generation
- LTE (Long Term Evolution) – theoretical speeds of 100
Mbps or more, actual download speeds 10-15 Mbps
- HSPA (High Speed Packet Access) – 14 Mbps downlink,
5.8 Mbps uplink;
- HSPA+ – theoretical downlink of 84 Mbps, 22 Mbps
uplink

# 5th Generation AKA 5G
- New radio multiplexing technology
- Utilizes more efficient spectrum usage
techniques
- New spectrum
- Allows up to 10 – 20 Gbits/s data rate
- Expected to provide support for immersive user
interfaces, such as AR (Augmented Reality)/VR
(Virtual Reality), mission-critical applications

- 5G won’t just support humans access the
Internet from their smartphone; expected
use cases include the following:
  - Control of home appliances, industrial robots,
  and even self-driving cars
  - Provisioning a large number of autonomous
  devices working together to provide service to
  end users
  - Allows interconnection of large number of
  Internet-of-Things (IoT)

# Bluetooth
- Bluetooth is a specification for short-range,
point-to-point or point-to-multipoint voice
and data transfer
- Bluetooth can transmit through solid, non-
metal objects
- Its typical link range is from 10 cm to 10 m,
but can be extended to 100 m by increasing
the power
- Bluetooth enables users to connect to a wide
range of computing and telecommunication
devices without the need of connecting
cables
- Typical uses include phones, pagers, LAN
access devices, headsets, notebooks and
desktop computers

# ZigBee
- Based upon IEEE 802.15.4 standard
- Used for low data transfer rates (20-250 Kbps)
- Also uses low power consumption
- Ideal for heating, cooling, security, lighting, and
smoke and CO detector systems
- ZigBee can use a mesh design – a ZigBee-
enabled device can both accept and then pass on
ZigBee signals

# Near-Field Communications (NFC)
- Very close distances or devices touching
- Magnetic induction (such as radio
frequency ID) used for transmission of data
- Commonly used for data transmission
between cellphones

# Topology
- Initially wifi networks were peer-to-peer, but this was unreliable. As such, there was a need to improve these types of communications in terms of speed and capacity to hold more nodes.
- We moved to full-duplex communications for Wifi, which is an improvement, but not widespread. 

## IEEE 802.11 Family of Standards
- Defines standards for wireless networking, and specifies the frequency, security improvements and improvements in bandwith.
- The most common one is 2.4 GHz or 5 GHz wifi. 6Ghz is rare but upcoming.
- With standards you can ensure that you'll have devices following the same protocols so that they'll be reliable for the user.

- 802.15: Wireless personal area networks, aka wireless version of PAN
- 802.16: WirelessMan and WiMAX

## Reminder: Hierarchy of networks
- PAN (Personal area network)
- LAN
- WAN
- WMAN (Metropolitan area network)
- Internet

## Understanding Encryption [(Video link)](https://www.youtube.com/watch?v=1y1M2fZqIlQ)

## How networks work (Internet, packet switched, etc)
- Packets are sent to a data center, that stores thousands of files
- Satelleites can be used to accelerate communication, which is what your phone transmits data to. 
- It's not great to purely use satelleites, because it's still a long distance for microwaves to travel. Data would have to travel thousands of miles. It's a complicated network of optical fibre cables that connect the mobile device to the network.
The server is a powerful consists of power devices that contain ssds that store stuff
- Uses Ips that send packets to the user.. etc.
- Data sent through optical fibres will be transmitted through light pulses, and usually installed on the sea beds between continents.
- ICANN -> Assigned names and ips for dns

## Electromagnetic waves
- The electromagnetic frequency range is extremely important
THey're sent from one tower to another.
Geographical areas are divided into cell. 
- Cell towers will send various signals to some cell towers, and other frequencies will be rejected by a chip on the cellular device
- Sometimes, during busy times/in busy areas you would not be able to call/communicate because of the overabundance of signals.
- Antaennae's send in various frequency ranges.
- Signals are usually encrypted so that attackers cannot listen in.
- The frequency band is limited though, and countries usually use similar spectrums.
- FCC governs the radio specture
- The ITU manages all technical issues related to the electromagnetic spectrum
- QAM -> You can send more bits at a time, which is done by mods done to the amplitude and phasing.
- Limited frequency ranges available for cellular communication:  758 - 802 MHz
: Frequency slots are distributed to different cell towers, and neighboring cell towers can not have the same frequency
- OFDMA -> Available range are divided into subcarriers

### Satelleite
- Satelleites are also regulated by government bodies, and are given a frequency range depending on the coordinates that they have/travel.  Many sateilleites are located at different areas, and transmit data at different frequencies so that devices may not receive the same data.
- Some navies will use protocols specifically for security. Services are rented in some countries, but ultimately communications have to be encrypted.
- Galapagos Islands had no internet before 2005, and sateilleites had to be installed on each island to ensure communication.

## Examples of satelleite communications
- Inside a city, towers are close to eachother. However, communications are difficult to maintain across cities. There's protocols for transmitting data through mountains or long distances, especially between different cities.
- Wimax (aka wifi on steroids) used to transmit data over different cities/countries. 
- Ex: A rural area can use Wimax to transmit data wirelessly through a satelleite. Not comparable to wifi networks at home, WiMax is a long-distance protocol to ensure signals can get far.
- Access point usually receives a communication here. 

## IOT devices in wireless networking
- There's a lot of wireless protocols for IoT. 
- ZGBEE is for IoT: They send messages to eachother as it if were a star network.
- You can create a mesh network of devices and transmit data through that mesh. 
- Ex: A traffic light is connected to a few nodes, which is connected to another traffic light. There are connections between these two lights that ensure that they can communicate.
- No need for an access point, you can rely on the mesh network.
- Companies (especially google) are creating protocols for wireless networks
- LoRA -> common for industrial IoT and small cities.