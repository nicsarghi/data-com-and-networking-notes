---
layout: post
title: Lecture 11
date: 2022/10/19
file: DataComm-Lecture11.pdf
dir: /lectures/
--- 

# The project
- The project will start next week! (Week of the 24th)
- The code is given, but you need to edit it

## The agenda for today :) 
( They're also common topics in Cybersecurity)
- Security goals
- Encryption
- Hash
- Digital signature
- Java code for providing security
- Digital certification

<br>

# Security goals
## Confidentiality: Protect sensitive data
- Prevent unauthorized entities from accessing the data (Privacy)
## Integrity: Ensure no authorized modifications 
- Protect data from unauthorized chagnes (Encryption)
## Availability: Authorized entity can access it
- Data be accessible to an authorized entity when it is needed (Uptime)

## Examples of how security goals are achieved
- Implementing and ensuring personal, system and internet security.
- Vulnerability/security management. Vulnerability refers to a security weakness in a system
- Using software tools and websites to ensure & measure security. (Stuff like Scanning tools, Network Defence Wireless tools, etc.)

## Security attacks
- Examples: Spoofing social media, phishing emails/sites

### **Confidentiality**
Snooping and traffic analysis 
### **Integrity**
Modification, masquerading, replaying and repudation`*`
### **Availability**
Denial of service  
<br>
`*` Repudation refers to spoofing websites and using false certificates. Non repudation is the opposite where you can make sure someone is legitimate because of the certificate they provide.  
<br>

# Encryption
 Encryption is the process of converting data into an unreadable format by scrambling the content so that it can be read only by someone who has the right **encryption key** to unscramble it.

## Terminology
- **Plain text:** Message that has not been encrypted
- **Cipher text:** Message that has been encrypted
- **Asymmetric key encryption and Symmetric key encryption** are two methods to encrypt a message 

## Symmetric key encryption
- **Same key** for encryption and decryption.
- Exchanging the key securely is a problem: it can easily be leaked. 
- **Faster** than asymmetric
<br>
![Symmetric key encryption](images/symmetric-key.png)

## Asymmetric key
- **Two keys** (Public and private keys for encrypting a message) are involved in the encryption/decryption process
- The **public key** is used to encrypt the message, the **private key** is used to decrypt the message
- Keys are manageable and there is **no need security exchange the public key**
- **Slower** than symmetric  
<br>
![Symmetric key encryption](images/asymmetric-key.png)

## Symmetric-key encryption algorithms
- The problem here is sharing the key.
- The algorithm consists of using the same key for both encryption and decryption. 

<br>

# Symmetric key algorithms
- **Block ciphers:** Encrypting by portion of data. You encrypt one block at a time (block sizes are usually 64 or 128 bits)
- **Stream ciphers:** Encrypting byte-by-byte, aka one byte at a time.

<br>

## Block cipher algorithms
### **Data Encryption Standard (DES)**
Use a 56-bit key to encode 64-bit blocks of data
- Insecure due to the small key size
- Key can be ascertain using exhaustive search within a few minutes

### **TripleDES**
- Uses DES 3 times in tandem
    - Output of 1 DES input to the next DES
- Not secure and therefore should not be used

### **Advanced Encryption Standard (AES)**, ***aka the most relevant one***
- Replacement of DES
- 3 possible key sizes (128, 192 or 256 bit keys)
- Uses 128 bit block size

**The size of the key is important in encryption, bigger keys tend to be more secure but they take more time to process. Keys today are bigger in size.**

## Stream cipher algorithms
- Encrypts and decrypts data faster than block ciphers

### **RC4**
- Widely known stream cipher
- Used primarily for WIFI 
- Used in WEP (Wired Equivalent Privacy) and WPA (Wi-Fi protected Access), as well as TLS/SSL
- Insecure and should no longer be used

# Asymmetric key algorithms
## RSA
 RSA is the most commonly used asymmetric-key
encryption algorithm
- Based on the difficulty of factoring prime numbers
- Developed by Ron Rivest, Adi Shamir, Len Adelman
- Supports variable length key sizes (e.g., 512, 1024, 2048,
3072, ...)
- Current recommended key size: minimum of 2048 bits

## Encryption algorithms key strength
- Cryptographic algorithms is determined by the length of the key in bits
- Set of possible keys for a cryptographic algorithm is called the **key space**
- Trying to crack the key would require a brute-force (aka trying all possible keys until you find the right one)

## Java AES encryption and decryption
- 6 modes of operation for symmetric key crypto schemes: each mode divides plaintext into 128-bit blocks
- ECB (Electronic Code Book)
- CBC (Cipher Block Chaining)
- CTR (Counter Mode)
- GCM (Galois/Counter Mode)

The NIST (National Institute of Standard and Technology) will recommend a schema for which algorithms to be used for encryption

# Integrity
## Ways to ensure the integrity of data
- Hash function
- Message Authentication Code (MAC)
- Digital signature

## Hash function
- Cryptographic hash function is an algorithm that takes data of any length and produces a fix-length output called a message digest or a fingerprint
- Hashes are one way. You can't decrypt the reuslt of a hash function. They're only used to compare to other hashes
- A good hash function needs to have next-to-no collisions (two inputs that have the same output)

## Example of hash functions
MD5, SHA256

## Message authentication Code
- A secret key is required to produce the MAC
- HMAC (hash-based MAC) is the algorithms that's used along w/ a secret key.

## Password Hashing
- Password hashing functions need to have a configurable hash-time parameter
- Argon2 is the recommended choice of hash functions that satisfy this requirement. It won the Password hashing Competition in 2015. It's the recommended algorithm to use for hashing passwords.

## Argon2 algorithm
- Argon2 accepts the following 5 parameters
- Salt lenght
- Hash length
- Iterations
- ...

## Java Argon2 Implementation
- We'll have to use it for the project, it's given in the project repository inside one of the example files.
- It uses the JNA (Java Native Access) to access the Argon2 C library

# Digital Signature
- The signing algorithm uses a private key to generate the signature
- The verification algorithm uses the signer’s public key to decrypt the
signature
- To reduce the size of digital signatures, the message to be signed is first hashed using a cryptographic hash function, then the message digest is signed

- Along with hashing data, you verify the hash by verifying the signature with a public key/private key pair
![Signing and hashing of a Digital Signature](images/certificate-hashing-and-signing.png)

## Digital signature schemes
- RSA
- DSA
- The recommended key length for both RSA and DSA signature schemes is at least 2048 bits
- ECDSA (Elliptic Cure Digital Signature Algorithm)
- EdDSA (Edwards-Curve Digital Signature Algorithm)
- The National Institute of Standards and Technology (NIST) in FIPS 186-5 recommends using **RSA, ECDSA and EdDSA**
- These algorithms use several mathematical functions for the encryptions (Elliptical curve, factorization, Edwards-Curve, etc.)

### Bonus
- In 2005, people began to digitally sign their documents by using private/public key pairs. I'm not sure what exactly Ponce was referring to, but there was a [Secure Electronic Signature Regulations act signed in 2005](https://laws-lois.justice.gc.ca/eng/regulations/sor-2005-30/FullText.html).

## Mathematical foundations for these algorithms
- **NOTE: This isn't in the xam**
- DSA algorithm is based on descrete logarithm problem:
    ```
    Given the equation β = g
    a mod p
    • If p is carefully chosen, it is considered very difficult to compute the value of “a”, given β,
    g and p
    • However, β can be computed quite efficiently if g, “a” and p are given
    • i.e., exponentiation modulo p is a one-way function for suitable p
    ```
- Elliptic Cure Digital Signature Algorithm:
    ````
    In 1985, Neal Koblitz and Victor Miller, independently proposed that elliptic curves can
    be used as the basic of public key cryptography
    • Motivation: discrete logarithm problem is much more difficult for elliptic curves over
    finite fields compared to over finite fields
    ````


# Digital Certificates
- Electronic document that binds the value of a **public key**, sender information and the length of time the certificate is to be considered valid.
- The point is to keep personal information about the entity to certify that the key belongs to them. 
- Issued by a certificate authority (CA), which is an entity who creates the certificate, and guarantees that it is valid. Browsers will usually identify these certificates, and warn the user if the certificate is invalid.
- Based on the trust that the client has for the certificate authority.

![Example showing Wikipedia's certificate](images/certificate-example.png)

## Certification authority
- Ponce made a certificate authority apparently for his thesis.
- Certificate authorities usually have a repository for certificates classified by country
- They organize it by domain names, and issues it to those who buy it from them.

![Certificate acquiring process](images/certification-authority.png)

## User and server authentication
- Commonly used authentication schemes
    - Passwords
    - **Digital certificates**
    - Radio-frequency identification (RFID) cards
    - Biometrics (Aspects of biology you can measure, so like thumbprints for example)

## HTTP certification
- Applications use certifications all the time. Web browsers and email for example.
- In HTTPS, for example:
    - A Web browsers validates (authenticates) an HTTPS Web site using the digital certificate (Asymmetric encryption)
    - The Web server sends a session key (i.e., single-use symmetric key)
    - Secure interaction between client (Web browser) and server (Web server)

 


