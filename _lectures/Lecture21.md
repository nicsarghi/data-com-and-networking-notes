---
layout: post
title: Lecture 21
date: 2022/11/24
file: DataComm-Lecture21.pdf
dir: /lectures/
--- 

# Cybersecurity Webinar [(Link to video here)](https://moodle.dawsoncollege.qc.ca/mod/url/view.php?id=520424)

## Main topics
- USB devices
- Social Engineering
    - Phishing
    - Vishing
    - Impersonation
- Liv attacks
- Protection measures
    - Data Backups
    - Passwords
    - 2FA/MFA
    - Online Accounts
    - Wi-Fi hotspots
    - VPN

# State of the art cryptographic algorithms
- Briefly mentioned, [a link is up on moodle but here it is](https://jusst.org/wp-content/uploads/2020/10/The-State-of-the-art-Cryptographic-Algorithms.pdf). It's down right now so [here's another one.](https://ijcsi.org/papers/IJCSI-9-2-3-583-586.pdf)

# USB Rubber Duck
- Keyboard injection tool. Monitors input from keyboards.
- The demo shows two cables, one is malicious and is used to plug into a phone. It turns the phone off and on, types in a passcode and goes on a website. It's because the circuits on the phone are used to manipulate input on the phone (The logic is that keyboards (that use USB) are trusted -> so computers trust keyboards.)
- USB keys can use 2fa hardware tokens. Don't share the private key because it's literally unsafe. A private key is useless once a picture of one is leaked. 

# Concordia AI institute
- AI launch lab: In person seminars for AI on saturdays for the AI challenge internship that's in march. It's a requirement if you want to participate in the AI challenge
- Dawscon -> challenges for programmers and non-programmers. 
- AI -> Machine Learning Reinforcement Learning