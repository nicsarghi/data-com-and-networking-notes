---
layout: post
title: Lecture 10
date: 2022/10/05
file: DataComm-Lecture10.pdf
dir: /lectures/
--- 

# Encryption

## Security goals aka the CIA triad
"Confidentiality, integrity and availability, also known as the CIA triad, is a model designed to guide policies for information security within an organization." [(Source)](https://www.techtarget.com/whatis/definition/Confidentiality-integrity-and-availability-CIA)

Keys for remembering: *CIA is a federal agency that wants a lot of secrecy, so any CIA agent should value* ***confidentiality, integrity and availability*** *when sending messages*

### **Confidentiality**
Think of privacy. You don't want confidential information to be accessed by the wrong people (aka unauthorized access) Some information can be dangerous in the wrong hands.
### **Integrity**
You want the data to be as accurate, trustworthy and consistent. Data absolutely cannot get corrupted nor altered by unauthorized people.
### **Availability**
Information has to be **consistently and readily accessible**. You need to maintain the hardware and infrastructure that sends this information.
### **Also important:** Non-repudiation 
Involves transmitting certificates to the client (SSL/TLS). It's defined as " Assurance that the sender of information is provided with proof of delivery and the recipient is provided with proof of the sender’s identity, so neither can later deny having processed the information." [(Source)](https://csrc.nist.gov/glossary/term/non_repudiation) 

## Some other goals, although not as relevant
- Reliability 
- Accessibility 
- Authentication
- Password management

# What is encryption?
- Remember from last semester of infrastructure!
- The point is to convert plaintext into cypher text (encrypted text)
- Encryption describes the process of converting encrypted text
- We usually use an algorithm and a key for the encryption process

## Two types of encryption algorithms
- Symmetric: Same public key for both sides
- Asymmetric: Public key and private key
- (Hashing is a third type, as there are 3 categories for NIST-approved cryptographic algorithms)

## Encryption algorithms
AES, 3DES (note that 3DES is likely to be retired)

# Encryption program
## PKI
- Messages are encrypted using private and public keys.
- You can also use certificates to generate private and public keys.
- Usually determined by a central authority, (aka certificate authorities)

## PGP
- You trust the public-key certificate belongs to who it says it belongs to. People have their own set of keys to "trust".
- Keys are usually bound to a username or email. 
- Think of openPGP or the pgp command.

## Hash
- Consists of getting a unique signature for plaintext, which an algorithm can compare with the same plain text and confirm whether or not their hashes match.

# Midterm go-over
## Question 5 of MCQ
The original answer was Physical, but some people put Data Link. The question was cancelled. Physical technically makes moer sense in this context because DL usually deals with MAC addresses

## Question 6 of short answers
```
a) a.b.c.d/23 -> The network portion of the IP address has 23 bits:
1111 1111 . 1111 1111 . 1111 1110 . 0000 0000
This means that the first 23 bits of the address are "taken up" by the network IP

This leaves us with 9 bits for the network (32-23 = 9, where 32 is the number of bits in an IP)

Since there are 9 bits left for the network, this means there are 9 slots for the possibilities in which a bit can occupy that space. A bit consists of only two values, which means that there are two possible choices for an overall of 9 slots. You can then find the number of possible nodes by knowing the number of possible hosts in that network:

2^9 = 512 hosts

```

```
b) This question is the backwards version of the one before it.
If you want 1000 IP addresses, then the number of hosts you theoretically would want would be 1024 (since it only goes up by exponents of 2). 
Note that 1024 is equal to 2^10

This means that there are 10 slots in which there is a possibility of either 1 or 0 being a number. Let's represent these slots by 0, and the rest of the slots by 1:
1111 1111 . 1111 1111 . 1111 1100 . 0000 0000

This indicates that there are 12 slots (bits) remaining for the network portion of the IP address. Since you now know the number of bits corresponding to the network, you can now write the CIDR notation as:
a.b.c.d/12

```