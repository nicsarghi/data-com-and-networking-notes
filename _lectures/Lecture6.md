---
layout: post
title: Lecture 6
date: 2022/09/14
file: DataComm-Lecture6.pdf
dir: /lectures/
---  

# Physical Layer
- This was defined earlier in the slides, But as a reminder, it's the first layer on the OSI reference model, defines the medium in which binary data travels through electromagnetic signals.
- We usually refer to the wiring or type of physical connections in this layer. Things like ethernet cables or wifi signals.

## Physical mediums for Ethernet 802.3
- UTP or STP (Copper cables, consists of 8 wires)
- Fiber (Uses light around a glass fibre to send signals)
- WiMax (IEEE 802.16) 

## WiMax and ranges
- The point is to determine a protocol that will allow for travelling long distances. 
- It's used mainly to provide mobile broadband connectivity across cities and countries. Wireless alternative to cable and DSL.

## Fiber optic
- Fiber optics had begun to exist in around the late 60s. First generation systems existed in 1977, and the second gen ones were produced in the beginning of the 90, with the bandwith per fiber increasing 

# Signals
- Electromagnetic energy impulses travelling through cables or air.
- Defined by a function that resembles a wave. 

# Analog signals
- Continuous waveform. Usually consists of sound (naturally occuring music and voice)
- More difficult to separate noise here compared to digital signals

-*Noise:* Unwanted electrical or electromagnetic energy that degrade the quality of signals.

# Digital
- Discrite or non-continuous waveform
- Appear in a fixed number of forms (looks more like a square function)
- You can discern a high voltages from a low voltage
- Too much noise is when you cant discern a high voltages from a low voltage

## Signal properties
- We use these properties to determine how data is sent. Digital signals, for example (varying voltages) can be consisted of data.

## Amplitude
- How much energy the signal has. 
- Shows the height of the wave above or below a reference point (in the function definiting it)
- Typically mesured in volts.

## Frequency
- How many signals you can send at a time (how often it oscillates between one pole to another)
- The number of times a signal makes a complete cycle within a time frame.
- **Spectrum:** Range of frequences that a signal spans from minimum to maximum. Defines a range of signals, ie the micro spectrum.
![Amplitude ranges](images/frequency.png)

### Bandwith
- Absolute value of the difference between the lowest and highest frequences of a signal
- function: abs(f1-f2)
![Amplitude ranges](images/amplitude.png)

### Example: Average voice
- Frequency range of 300 Hz to 3100 Hz
- Spectrum would be 300-3100 Hz
- Bandwith is 2800 Hz AKA abs(3100-300 Hz)

## Phase
- Describes how the signal changes position. Calculated relative to a given moment of time or relative to time 0.
- A change in phase can be any number of angles between 0 and 360 degrees. 
- Phase changes occur on common angles like 45, 90, 145, etc.
- Angle is defined as how a sine function would map it out. A phase change at 180 would be the change in position between one pole to another (-1 to 1)
![Phase change](images/phase_change.png)

## Baud rate & bit rate
- Serial communications are at a predfined speed
- This speed is referred to is baud rate, AKA the number of bits transmitted per second
- Frequently used is 9600 bps

## Linux Commands Replacements ([Link to article](https://ubuntu.com/blog/if-youre-still-using-ifconfig-youre-living-in-the-past))
- Ponce DID NOT provide the original link, but here's some replacements for ifconfig, which is outdated. The article suggests using the ip command

## Modulation ([Link to video](https://www.youtube.com/watch?v=mHvV_Tv8HDQ))

# OSI layer and protocols table
- Microsoft uses the first four layers of the 4 OSI model, usually for the low level software and drivers
- Most devices simplify the OSI model into TCP/IP protocol suite  
![OSI model table first half](images/osi-model-1.png)
![OSI model table second half](images/osi-model-2.png)

# Internet layer
- We generally know about the IP layer the most, so this part is going to just be a reminder
- Allows you to address packets in a network. This is why nodes have an IP address.
- The layer (and first one) that addresses the problem of transmitting data from a specific node (source) to another specific node (destination).
- Since it's logic based, you can create virtual circuits (predefined paths between two computers.) This is because IP addresses refer to a logical address assigned to a node (instead of it because a network card)
- Internetworking is achieved through routing.
### The Internet Layer provides protocols that are responsible for addressing and routing of packets

## IP protocols
- Internal Protocol (IP)
- Internet Control Message Protocol (ICMP)
- Internet Protocol Security (IPSec)

### IPv4 packet header
![Ipv4 packet header](images/ip-packet-header.png)
### IPv6 packet header
![IPv6 packet header](https://docs.oracle.com/cd/E19683-01/806-4075/images/HeaderFormat.epsi.gif)

## Decentralization
- IP networks are decentralized and dynamic (there's no network that manages the entire protocol. )
- A packet might not always reach a destination node (or the packet might be altered, aka its not the original packet). This is why a network node will use a checksum to tell if a packet header is changed or not.

## IP packet checksum
- The manner in which the protocol ensures the order of the packets are sent correctly
- It verifies the order in which the packets are sent, usually how they are transmitted (The transport layer uses information from the IP packet)

## IPv4 vs IPv6
- Devices on IP based networks need IP addresses. This includes the internet
- IETF (Internet Engineering Task Force) develops and promotes Internet standards. They're the masterminds behind IPv4 and IPv6
- Internet Assigned Numbers Authority (IANA): They coordinate IP addresses and resources around the world. The primary address pool of IPv4 was announced by them on Feb 3 2011.
- Slow transition to IPv6

## Classful Network classes (IPv4)
- Archetecture that defines the addresses and how they divide them into 5 different types of networks
- Original five classes: A, B, C D
- ABC are used for networks
- Each class is restricted to an IP address range. They're essentially a standard that most companies will respect (ie by reserving some IPs as "private network only")
- The ranges are determined based on how many nodes you might need. A allows for more addresses in a network contrary to C
- Network mask matching consists of a logical and (to get the network you do HOST_IP & NETMASK)
![Network Address types](images/network-address-types.png)

## IPv4 CIDR and Subnet Mask
### CIDR
- CIDR (Classless inter-domain routing)
- Replaces the classful network architecture
- Temporary solution for IP address shortage, provides more ranges for networks
- Similar to IP address dot notation
- Networks are split into groups of IP addresses called CIDR blocks
ex: 168.12.0.0./16 -> the first 16 bits define the network. 

### Subnet Mask
- Binary number that contains all 1s in the elftmost prefix length positions
- Subnet mask for the CIDR address block 168.12.0.0./16 would be 255.255.0.0 (first 16 bits!)

## IPv4 Private Networks
- Contains private IP addresses
- Not routable on the Internet
- The purpose of private IP addresses is to allow orgs to asign their private IP addresses to nodes on the network using the IPs in the private network ranges
- NAT (Network Address Translation) maps internal addresses to public routable addresses
- Port Address Translation allows you to share a single, public-facing IP address with a range of internal private IP host addresses

## Address Resolution
- Process of finding an IP address from a given hostname
- DNS is a hierarchical naming system that allows orgs to associates host names to IP addresses
- DNS servers store associations between the name and ips. Some requests will redirect to other DNS servers
- They usually keep up with changing host names. They also react to organizations that change their IP addresses
- Needed to make the Internet usable (since hostnames are more easier to remember than IP addresses)

# IPv6
- The point is to increase network address space. We've already exhausted the amount of IPv4 address spaces in 2011
- 128 bit addresses instead of the IPv4 32 bit addresses
- 8 groups of 4 hex numbers.
- Literally 2^128 possible addresses. Even your neighbor and his dog can have an IPv6 address at this point.
- First 64 bits identify network
- Last 64 bits identify host (based on the mac address)
![IPv6 address format](images/ipv6-address-format.png)

## IPv6 compression
- Ipv6 address can be shortened by dropping leading 0s in each group
```
    2001:0db8:0000:0000:0000:0053:0000:0004
    |
    v
    2001:db8:0:0:0:53:0:4
```
- You can drop a single zero and replace it with nothing
```
    2001:0db8:0000:0000:0000:0053:0000:0004
    |
    v
    2001:db8::53:0:4
```
(Note that only one set of :: can exist in an address)

## Network methodologies
Beyond the scope of this course, so we'll briefly touch it.
- Unicast: Sending a packet to a single destination
- Anycast: Sending a packet to the nearest node in a specified group of nodes
- Multicast: Sending a packet to multiple destinations

**A broadcast address can send a packet to a complete range of IP
addresses**

### Dual IP stack OS support both IPv4 and IPv6 using two separate network stacks for IP

### Literal IPv6 Addresses
- The colon character is a reserved character (for the network resource identifiers) in the Universal Naming Convention path names. 
- For network resource identifiers, add square brackets around the literal IPv6 addresses (``http://[2001:db8::206:0:a80c:52b]:8080``)
- For UNC path names, convert colon characters to dashes and
append “.ipv6-literal.net” domain to IPv6 literal addresses
```
    2001:db8::206:0:a80c:52b
    would be written as
    2001-db8--206-0-a80c-52b.ipv6-literal.net
```

## ARP cache table and IP Routing Table
- The network stack needs to know the packet's local destination (once the packet enters the destination network.)
- IPv4 use ARP to map IP addresses to MAC addresses. The network will usually cache mac addresses and store them in an IP table.

- IPv6 networks use the Neighbor Discovery Protocol (NDP)
- ARP and NDP make it possible for packets to send data to IP addresses inside the nwtwork, and also transport that data between network. 
- Routers in a linux box will use the eth0 net interface, This is logical, so you can have multiple IPs for it

## Connectionless vs Connection-Oriented Communications
- The most  direct path to a destination may not be the best route if it’s congested. 
- IP is a connectionless protocol:
    1. No notion of a connection between source and destination nodes
    2. Treats each packet as its own separate unit.
- The alternative to this is a connection-oriented protocol:
    1. It sets up a connection between the source and destination
    2. Treats each message separately

- Layered networking software enables a mix of connectionless
and connection-oriented protocols
- Can use a connection-oriented protocol at the Transport
Layer and still use IP for the Internet Layer

# Internetworking
- Achieved via MAC layer bridging and network layer routing

## Physical and logical netowkr configuration
- Networking is possible on the virtual level (think of cloud-based solutions), meaning that you're able to configure a Virtual LAN for individual devices on that VLAN

![Physical and logical networks](images/vlan-lan.png)

# Routing
- Routers operate on the network layer. 
- It's the same thing as a layer 3 switch. It also is used for WAN circuit connections, campus backbone connects and building backbone connections

## Network layer address
- Routers can also make intelligent decisions on where to send packets (dynamic routes, for example)
- Instead of reading the MAC addresses and forwarding to that mac based on a forwarding table, it can see the packet's Network Layer address is ( or the IP address)
- Network Layer addresses contain information about the destination network number and host number
- Performs a path determination calculation to find the best path for the packet to traverse

## Internetworking with a Gateway
- Interconnect two networks that will use different protocols
- It will also translate network packets from one network protocol to another
- Main job is to translate all incoming packets to a protocol compatible with the destination network
- Placed at the entry and exit points of a network
- Runs as software on a computer or as a device that performs the same functions as a router

## Static route
- Going to a router and manually determining static IPs in a protocol
## Dynamic route
- Protocols that determines therroutes between the IPs (OSPF, RIP)
- Analyses the flow of traffic between the routers. Internally updates a table that will determine what route to take next.
- Sometimes finds the shortest path to the destination through the routers

## Resiliency and Redundancy
- No need to go in detail (Ponce mentioned that the theory is much too detailed for the class)
- There are protofcols controlling the resiliency and redundancy of a netowrk (analyzing the shortest path for redundancy)

### Redundancy
- A way to have alternate paths
- Ways to guarantee the path transmits data efficiently. Avoids interruptions in network comm.
- Layer 2 Resiliency is done by enabling resiliency protocols, such as Spanning Tree Protocol (STP) and Resilient Ethernet Protocol (REP), to avoid network communication interruptions
- Layer 3 resiliency supports alternate routes to a WAN when the
primary route is unavailable (Example: Cisco Hot Standby Routing Protocol or HRSP)
- Virtual Networking Components is when you implement some network devices as virtual machines (VMs) or containers, or when you build entire virtualized networks
- Network Storage Types consist of implementing network storage devices as VMs;
storage area networks and network attached storage

# Transport Layer
- The point is to determine a connection between a sender and receiver (in the case of TCP), usually to determine how packets are sent and re-ordered

# Transmission Control Protocol (TCP)
- TCP sets up a connection and sends acknowledgments
(ACK)
- TCP guarantees delivery
- TCP provides connection-oriented, reliable communication

# User Datagram Protocol (UDP)
- UDP doesn’t set up a connection and doesn’t send ACK
- UDP can move data faster, but not guarantees delivery
- UDP provides connectionless, unreliable communication

## Port
- Achieved through software. Almost entirely logical and not physical
- Exists to process requests for data or services
- Ports and sockets are used to receive and send data. A socket is the endpoint of a specific connection.
- As a packet is moving up through the protocol stack on its way to the Application layer, the Transport layer directs the packet to  appropriate port
- A port **is a number that the application atthe Application layer uses as a send-and-receive address**
- Applications listen for request at the ports to which they are assigned
- TCP and UDP each have a range of 65 536 ports to choose from. Some are reserved for specific services, some are not.

## Socket
- Processes requests for data or services
- Used in combination with ports to receive and send data
- The socket is the **endpoint of a specific connection**
- A socket combines three pieces of information:
    1. The IP address
    2. The type of transport (TCP or UDP). This indicates which protocol is to be used
    3. The port number
- Sockets can be written in the following fashion: ``198.168.48.36:TCP:80`` or ``198.168.48.36:UDP:53``