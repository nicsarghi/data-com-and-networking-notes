---
layout: post
title: Lecture 9
date: 2022/09/28
file: DataComm-Lecture9.pdf
dir: /lectures/
--- 

# Some notes on the final 
The final will be more difficult and have more java coding questions
Most questions are theory based.

# Notions to keep in mind for the upcoming project
- Pi4J
- TilesFX(JavaFX)
- Call system
- Parsing
- Data communications (?)
- MQTT
- Junit

## Circuits
- Read the freenove manual for learning about circuits, do the tutorials, etc. 
- IntelliJ works slightly better with renaming projects tha Netbeans.

## Javafx
- The Review slides are on Moodle, it's necessary for the project so make sure to review them :)
- Parsing is also a key component because most protocols use parsing to convert data into a more useful format. It's an important facet of data communications, since it's a key part in the process of transmitting data.

## The point of the labs
- Setting up the PI is more for the project, not really the exam.
- The networking labs are more important. Study those.
- AKA the ARP protocol, for example. Revise that!
- Be aware of what commands to use for the pins on the pi during the midterm
- IP addressing and subnet masks are an important part of the lab as well. That entire switch section

## CIDR
- Refers to the number of bits in the IP address that divided the network and host

# Practice test
1. What is an advantage of using DHCP in a network environment?
````
Answer: Assigns IP addresses to hosts
````
Instead of a network administrator manually assigning IPS, DHCP allows for the automation of it.

2. You need a topology that is scalable to use in your network. Which of the following will you install?  
```
Answer: Star
```
Bus and Ring are outdated, and Mesh is too costly (because you increasingly have to create more links each time you add one node to the network)


3. Which of the following is a concern when using peer-to-peer networks?  
````
Answer: The security associated with such a network
````
The main concern here is the fact that peer-to-peer networks share responsibilities and permission across all nodes in the network, meaning that they all have to trust eachother in that network. Since a large part of security involves setting the permissions for each user, this would be a concern because of how permissions are inheritely lax.

4. What advantage does the client-server architecture have over peer-to-peer?
```
Answer: All of the above
```
Easier maintenance, greater organization and tighter security are all advantages because of the centralized nature of the server, which has control over what's being sent over the network.

5. Which type of topology has the greatest number of physical connections?
````
Answer: Mesh
````
This is because Mesh topology networks, by principle, require interconnected nodes. Since every node is connected to eachother (it quite literally the has the maximum amount of nodes possible in a network)

6. Segmentation of a data stream happens at which layer of the OSI model?
```
Answer: Transport
```
This is because the transport layer deals with the physical connections and how data is distributed through signals.

7. Whcih IEEE standard specifies the protocol for CSMA/CD?
````
Answer: 802.3 
````
Trick for remembering the number: it always starts with an 8, and 3 never goes before 2.

8. What protocol is used to find the hardware address of a local device?
```
Answer: ARP
```
ARP is used to determine the MAC addresses of a device through an IP address. A switch typically stores an ARP table and determines the IP that way.

9. Which of the following addresses is not allowed on the Internet?
````
Answer: 172.31.12.251
````
This is because addresses reserved from the range 172.16.0.0 to 172.31.255.255 are private class B IP addresses reserved for private networks.

10. Which of the following is a valid Class B address?
```
Answer: 129.1.1.1
```
Class B addresses are considered to be with a range of: `[ 128-191 ].[ 0-255 ].0.0`
