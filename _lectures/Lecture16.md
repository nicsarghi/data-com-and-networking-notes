---
layout: post
title: Lecture 16
date: 2022/11/09
file: DataComm-Lecture16.pdf
dir: /lectures/
--- 

# Disclaimer for notation
- Note that hexadecimal values will start with a 0x. (ie. 0x0F to represent the number 15)
- It's assumed Unicode code points are represented by hex values, so no 0x will preceed them. Instead the value itself is simply displayed (ie. U+000F) 

# Application Layer
- The topmost layer of the OSI model (layer 7), same w/ the TCP/IP model (layer 4)
- Most of the theory below is abstracted, as everything done in the application layer is logical. As such, it's the only layer that's interacting directly with a person
- Is where the front-end software
interfaces with the computer user, often
referred to as the user interface
- The process to exchange a message with
another computer generally starts with a
Layer 7 software call
- Serves as the primary interface for
application software to interact with
networks

## Key Concepts
- Application layer of the OSI and
TCP/IP reference models
- How application architectures affect
network traffic
- Network ports and sockets
- How the web defines the core of
today’s online interactions
- Data encoding scheme
- Compression
- Error detection

## Common Layer 7 protocols
- Dynamic Host Configuration Protocol (DHCP)
- File Transfer Protol (FTP)
- Hypertext Transfer Protocol (HTTP)
- Network File System (NFS)
- Real-Time Transport Protocol (RTP)
- Session Initiation Protocol (SIP)
- Simple Mail Transfer Protocol (SMTP)
- Simple Network Management Protocol (SNMP)
- Telnet

## Application Architecture Types and Examples
- Host-based applications
- Client-based applications
- Cloud computing/n-tier applications
- Peer-to-peer applications

# Ports and Sockets in the Application Layer
- In addition to sending a request to a web server, a service
requester (i.e., a client) also provides a port number with
the request
- A port is a number that tells the server what type of
service the client is requesting
- On the server, many programs run as services, which are
programs that monitor a specific port for requests
- When a server receives a request for a service at a
specific port, the Application Layer passes the request to
the service listening on the specified port. An instance of
a service listening for traffic on a specific port is called a
socket

## How Web Applications work 
- **Web browser:** Downloads and interprets (parses the content) of webpages
- **HTML:** Formatting markup language of most webpages. This is what the browser parses (along with the JS and CSS that it will download)
- **Hyperlinks:** Text, media and links to external sources
- **Uniform Resource Locator:** Allows browsers to locate data from remote resoruces that are retrieved through hyperlinks

# Data Transmission 
- **A communication channel refers to the medium used to convey information from a sender (or transmitter) to a receiver**
  - Examples of channels: copper wires, optical
fibers or wireless communication channel

### Reminder: Types of network architecture
- Centralized
- Non-centralized (aka P2P)

## Types of communication
- You've seen this in the Data-link layer, but it's not necessarily involved physical. The way the channels are set up usually is logical:
    - Simplex: Only one direction, one time)
    - Half-duplex: Can't send and receive at the same time, but can do both)
    - Full-duplex: Can send and receive at the same time)

## Simplex
- Data in a simplex channel always flow in
one direction only and never flow back
the other way
-  Examples:
  - Interface between the keyboard and the computer
  - Broadcast television

## Half-duplex
- A half duplex channel can send and receive
data, but not at the same time; only one end
transmits at a time, the other end receives
- Example:
  - Telephones, fax machines

## Full-duplex Transmission
- On a full-duplex channel, data can be transmitted
in both directions on the signal carrier at the
same time
  - There is no need to switch from transmit to receive
mode like in half duplex
  - Example: Full duplex switch

# Encoding

## Data Representation
- We all know most communications are encoded into binary, which are then converted into electrical signals. 
- We end up using strings (aka standard characters) to represent the data once they are decoded from bits. Since this is the app layer, we represent data.
- We use ASCII (8-bit) or Unicode (16 bit, uses planes as well) to represent this type of data that will be transmitted over the network. Most people use Unicode nowadays since ASCII is limited.

## Data encoding schemas
- ASCII
- Extended ASCII (ISO 8859 family)
- Unicode
- Unicode Transformation Formats (UTF-8 and UTF-16)

# ASCII  

The American Standard Code for Information
Interchange (ASCII)
- It's a character encoding standard that uses **7 bits codes**, meaning that there are 128 codes
- The 8th bit is unused, but it *can* be used as a parity bit
- 95 of the codes are “printable characters” codes (displayable on a console)
- 33 are “non-printable control” codes (control features of the
console or communications channel)

## Control Codes
- Code values 0x00 to 0x1F, and 0x7F (0 to 31, and 127) are used in telecommunication.
- Control codes are meant to control the transmission of data between the terminal and a host. 
  - ETB (End of Transmission Block) and CAN (Cancel)
- It can also be used to control how data is presented 
  - tab, line feed, new line...

## Display Codes
- Codes 0x20 to 0x7E ( 32 to 126 )
- Examples include:
  - Space, punctuation and *some* special symbols
  - Letters (lowercase AND uppercase) and digits

## Limitations of ASCII
- Very few characters can be represented, and is only really meant to display characters present in the english language. Obviously not great if you want to start using characters from other countries (ie Cyrillic, Hangul, etc..) 
- Not many special characters such as the cent symbol


## SEE: Extended ASCII
- Also known as: ISO 8859 (International Organization for Standards #8859)
- An encoding scheme to provide additional
characters from the extra bit added to the already
existing 7-bit ASCII code
- There are sets 1 (ISO 8859-1) and more recently set
15 (ISO 8859-15) which are used to represent most
western European symbols)
- Other sets in between include set 2 (ISO 8859-2)
use to represent most eastern European symbols
and set 10 (ISO 8859-10) used to represent
Nordic/Inuit symbols
- Different systems end up using different code pages for the extra characters
- Code page:
  - Basically what's used for the 8th bit to encode everything
  - Identical only in the first 128 codes (the ASCII part)

## Limitations of Extended ASCII
- Still not enough to represent the thousands of characters that some Asian languages have
- You can't interchange documents between computers that use different code pages.

# Unicode
- A character coding system designed to support
processing and display of texts from diverse
languages
- Each character symbol is represented by a code
point
  - A code point is a theoretical concept
  - It does not represent how the character is stored in
memory
- Code point representation in memory is dependent
on the encoding scheme (UTF-8, UTF-16)

## Code Points
- The number of code points that can be
represented in Unicode is 1,114,112 (0x000000 to
0x10FFFF)
- A continuous group of 65,536 (= 2^16 ) code
points is referred to as a plane 
- There are 17 planes
- Plane 0 is referred to as the Basic Multilingual
Plane (BMP) and it is intended for modern use
- Code points in the BMP use 4 hexadecimal
digits and are represented with the notation
U+XXXX1
- Code points outside the BMP use 4, 5 or 6
hex digits and are represented with the
notation U+XXXX , U+XXXXX or
U+XXXXXX

### Example
- The English letter A is represented as ```U+0041```
- The string “Hello” is represented by the code
points  
``` U+0048 U+0065 U+006C U+006C U+006F ```

# UTF-16
- All characters in modern languages are
represented with a two-byte codes
- A Unicode byte order mark determines the
order in which the bytes are written
- Big Endian defines the byte order in memory
where the most significant byte is first
- Little Endian defines the byte order in
memory where least significant byte is first

### Example
- Encoding for “Hello” in UTF-16BE and UTF-16LE
  - Big endian: ````0x0048 0x0065 0x006C 0x006C 0x006F````
  - Little endian ```0x4800 0x6500 0x6C00 0x6C00 0x6F00```

- **Tldr:** **L**ittle endian always goes **Left**. And then Big Endian always goes **Right**
- "In order to decide if a text uses UTF-16BE or UTF-16LE, the specification recommends to prepend a Byte Order Mark (BOM) to the string, representing the character U+FEFF. So, if the first two bytes of a UTF-16 encoded text file are FE, FF, the encoding is UTF-16BE. For FF, FE, it is UTF-16LE." [(Source)](https://stackoverflow.com/questions/701624/difference-between-big-endian-and-little-endian-byte-order) 
- Big Endian byte order mark is ````FE FF````
- Little Endian byte order mark is ```FF FE```
- UTF-16 byte order mark indicates the order in
which the bytes are written

# UTF-8
- The most popular type of Unicode encoding
- It uses:
  - one byte for standard English letters and symbols,
  - two bytes for additional Latin and Middle Eastern
characters, and
  - three bytes for Asian characters
  - Any additional characters can be represented
using four bytes
- UTF-8 is backwards compatible with ASCII,
since the first 128 characters are mapped to
the same values
- Encoding is byte oriented so there is no
big/little endian problem
- The byte order mark is EF BB BF

# Breadboard Connections
You know how electricity works? When you connect a wire to a power source and into the ground, then you basically end up with a circuit.  
<br>
![Basic circuit](https://www.quickstudylabs.com/Electronics%201/FreeSafetyClass/Pictures/circuit_light.gif)  
The interesting part about a breadboard is that there's not really any wires. Instead, there's metal strips are what allows you to connect two components together.  
In fact, if you open up a breadboard, you'll see pretty quickly which parts are connected where:  
<br>
![Breadboard](https://cdn.sparkfun.com/assets/3/d/f/a/9/518c0b34ce395fea62000002.jpg)

Look closely at the breadboard and what's being noted. If you see the numbers denoting each **row**, then you'll notice that each row represents a copper strip that is able to conduct electricity.  
**This means if you plug a wire from 1a to 2a, both rows 1 and 2 will be connected.**  
<br>
![Breadboard connections](images/breadboard-connections.png)

Now, when you look at a schematic, you can see how they're able to connect a circuit in the breadboard.  
<br>
![Breadboard circuit](images/breadboard-example-circuit.png)

You can also look at the schematics for the circuit to get a better understanding of how they work [(Here's a guide on how to read electric schematics if you don't know how)](https://learn.sparkfun.com/tutorials/how-to-read-a-schematic/all)  
<br>
![Electric schematic](images/electric-schematic.png)



