---
layout: post
title: Lecture 17
date: 2022/11/10
file: DataComm-Lecture17.pdf
dir: /lectures/
--- 
This is a continuation of the Application Layer
Credit to Carlton for additional information from the slides.

# Data compression
- When two nodes communicate, the data can be exceedinly large. You'll need to compress the data to send smaller packets over the network
- Compression is a technique used to squeeze more data over a communications line or into a storage space
  - Basically, the smaller a file is, the easier it is to send over a network, since it'll take less time *to* transfer.

### Types of compression
- Huffman codes, run-length compression, Lempel-Ziv compression are ways to compress files
- Audio is aslo mostly compressed using FLAC (MP3 players and phones)
- Videos also use MPEG for compression

### Groups of compression
- Lossless compression: Uncompression does not lose data
  - Compressing sensitive information such as financial data
- Lossy: When data is compressed and then uncompressed, a portion of the original data is lost. (Lossy -> Loses a little) 
  - Compressing large files where a slight loss of quality is fine (ie audio, photos, movies, etc.)

## Lossy Compression
- Audio and video files do not compress well
using lossless techniques
- And we can take advantage of the fact that the
human ear and eye can be tricked into hearing
and seeing things that aren’t really there
- So, we can use lossy compression techniques on
audio and video (provided that we don’t lose too
much of the audio or video!)

## Compressing Audio
- MP3 players found in cell phones and iPod-like
devices store and play compressed music
- Audio compression is difficult to describe based on perception, which is why it's used 
  - For example, a louder sound may mask a softer
sound when both played together (so drop the
softer sound)
- Some people don’t like compressed audio and
prefer to store their music in lossless form (such
as FLAC), but this takes more storage space on the file system

## Compressing Video
- Video (both still images and moving video) does
not compress well using lossless compression
  - When examining the pixel values in an image, not
  many are alike
- But what about “from frame to frame” within a
moving video?
  - The difference between video frames is usually very
  small
  - So, what if we just sent the difference between
  frames?

## MPEG
- MPEG (Motion Picture Experts Group) is a
group of people that have created a set of
standards that can use these small
differences between frames to compress a
video (and audio) to a fraction of its original
size
- JPEG (Joint Photographic Experts Group)
  - Compresses still images
  - Lossy

# Noise and Errors
- Noise is always present
- If a communications line experiences too much
noise, the signal will be lost or corrupted
- Communication systems should check for
transmission errors
- Once an error is detected, a system may
perform some action
- Some systems perform no error control, but
simply let the data in error be discarded

## White noise
- Ie thermal or Gaussian noise. It's relatively constant and can be reduced
- It's commonly caused by the thermal vibration of atoms in conductors (ie heat)
- Ususally white noise is unwated noise in the signal. If the white noise is too strong, it will corrupt the data in the signal ( which is generally undesirable).  
<br>
![White noise](images/noise.png)

## Impulse noise
- One of the most disruptive forms of noise
- Random spikes of power that can destroy one or
more bits of information
- Difficult to remove from an analog signal
because it may be hard to distinguish from the
original signal
- Impulse noise can damage more bits if the bits
are closer together (transmitted at a faster rate)  
<br>
![Impulse noise](images/impulse_noise.png)

## Parity Checks
- If data is corrupted during the transmission, then parity checks can be used to correct that data (to avoid corruption)
- Digital Signals with Jitter can be detected, which can be detected when analyizing the frequency.
- More info on next lecture...