---
layout: post
title: Lecture 20
date: 2022/11/23
file: DataComm-Lecture20.pdf
dir: /lectures/
--- 
# Network Infrastructure Implementations

## Key concepts
-HCP, DNS, NAT, NTP
- Network
infrastructures for
enterprise, mid-tier,
and small networks
- Network access control

# DHCP (Dynamic Host Control Protocol)
- Your router has a range of private IPS, and will use DHCP to give a computer an IP
- Allows a network system administrator to automatically
allocate IP addresses to endpoints when they connect to
the LAN upon startup
- DHCP server listens for a broadcast Ethernet frame upon
which it responds with an IP host address to be used at
Layer 3; referred to as the DHCP discovery process
- Typical deployments have the endpoint reserve the same
IP address number during a leasing time period
- Once lease expires, endpoint receives a different IP
address from the DHCP server

## ASsigning IP addresses

- An IP address can be assigned to a
workstation permanently (static
assignment) or dynamically
  - Dynamic IP address assignment is a more
  efficient use of scarce IP addresses
  - When DHCP client issues an IP request, DHCP
  server looks in its static table
- If no entry exists, server selects an IP address from
available pool

## DHCP Lease
- The address assigned by DHCP server is
temporary
- Part of agreement includes specific period
  - If no time period specified, the default is one hour
- DHCP clients may negotiate for a renewal before
the time period expires

# DNS
- The point is to translate hostnames or domain name to an ip address

## Domain name space
- Hierarchical name space organized as an inverted tree structure.  
<br> 
![DNS tree](images/dns-tree.png)
- A request goes through the DNS server, then through the ca namespace, then through other namespaces inside of it.
- Some domains only exist for countries (.jp, .ca, etc.), some exist for other reasons (.org, .com)

- Domain name is sequence of labels
separated by dots “.”

## Fully Qualified Domain Name (FQDN)
A domain name that specifies its exact location
in the tree hierarchy of the entity:
- e.g.  : dawsoncollege.qc.ca

### Inverse domains
An inverse domain consists of mapping an address to a domain, usually when a server gets a request from a client.

### Country domains
Specific to a country.
![Country domains](images/country-domains.png)

### Generic domains
Domains that are not associated with a country, ie: .org, .com, .biz... 
<br> 
![Generic domains](images/generic-domains.png)

## Top-Level DNS servers
- A root name server is the name server for the root zone of a DNS. Requests for Top-level domains pass through here first.
-Top-level domain (TLD) DNS servers are responsible for top-level domains:
  -  gTLD: com, org, net, edu, etc.
  - ccTLD: ca, us, fr, uk, cn, etc.
- ICANN/IANA delegates to each TLD, meaning that they handle the responsibility of handling domain names to those top level domains.

# Network Address Translation
- Allows you to translate a public IP to a private IP. The router represents the local area network to the Internet as a single IP address
  - All traffic leaving the LAN appears as originating from a global IP address
- Used to send data to the rest of the Internet.
- It also hides all the workstation IP addresses from the internet
- Since the outside world cannot see into
LAN, you do not need to use registered IP
addresses inside the LAN.

## Reserved Private IPs for LAN
- 10.0.0.0 – 10.255.255.255
- 172.16.0.0 – 172.31.255.255
- 192.168.0.0 – 192.168.255.255

## How NAT is achieved
- When a user sends a packet that's outside of the local network, the router first changes the address in the headre of the IP packet to match the global IP. It then caches that change for later.
- When there's a response, it looks at the cache and switches the address back
- If the address mapping isn’t in the cache, the packet
is dropped (*Unless* NAT has a service table of fixed IP address
mappings)

# Network Time Protocol
- A protocol that synchronizes the clocks of all computers, routers and firewalls in a network. Can used for coordination between multiple timezones
- Is enabled in most operating systems, including
Linux, UNIX, and Windows
- Is essential to maintain integrity of audit trails
and logs
- Date and timestamps must be accurate to
perform proper forensics investigations
-   National Institute of Standards and Technology
(NIST) provides multiple NTP time clock servers
throughout the continental United States 

# Network Device placement
## Hub
- Operates at the Physical and Data Link layers
## Bridge
- Is typically used when more than one Ethernet LAN is needed to interconnect, such as in a multistory building
## Switch
- Is typically used for departmental workgroup LANs or a backbone network environment
## Router
- Used in the core backbone or in a WAN environment
## Firewall
- Located at the network’s ingress/egress point
(or where the Internet access link resides) or
between internal networks or web servers
## Intrusion decetion system
- Typically installed in front of or behind an IP
stateful firewall
## Media convertera
- Physical Layer device
## Modem
- Typically locafted where the analog phone line is terminated onto an RJ-11 phone connector
## Wireless access point (WAP) 
- Is located in hard-to-reach but omnidirectional
locations that maximize physical coverage
## Wireless range extender/amplifier 
- Is located outdoors or close to outdoor space
## Internet of THings (IoT) endpoint
- Found throughout a building facility wherever
HVAC and environmental devices are installed
## Voice over Internet Protocol VoIP endpoint
- Is located at the workstation area

# Access Controls
- Network access controls for local and wireless LAN (WLAN) access
 Local access controls when sitting in a workspace
- Wireless access controls when mobile at a workplace
- Local access with multifactor authentication
- Network access controls that support remote access with
multifactor authentication
- Remote access to a web-hosted Software as a Service
(SaaS) application or secure portal (e.g., HTTPS, with
updated digital certificates) that requires access controls
with multifactor authentication

# IAAA
## Identification
- Process of
confirming a
specific user
## Authentication
- Method for
verifying the
user is who
they say they
are (PIN,
token, etc.)
## Authorization
- Means the
user is
preapproved
to gain
access
## Accouting
- Refers to
continuous
monitoring

# Performance-Enhancing Devices
- Load balancer
- Proxy server
- Wireless LAN controller

## RADIUS vs LDAP: Brief Mention
- Both LDAP and RADIUS are for authenticating users.
- LDAP is a directory service that is used to search and modify directories over a network, such as those created by Microsoft® Active Directory® service. An LDAP server contains the directory of users in an LDAP directory tree. [(Source)](https://selinc.com/api/download/110067/)
- RADIUS is a protocol that allows for centralized authentication, authorization, and accounting (AAA) for user and/or network access control. RADIUS clients contact the server with user credentials as part of a RADIUS Access-Request message, and the server responds back with a
RADIUS Access-Accept, Access-Reject, or Access-Challenge message. [(Source)](https://selinc.com/api/download/110067/)

### Reviewing network infrastructure
- A router can be used to connect two LANS  
<br>
![Two lans connected by a route](https://www.researchgate.net/publication/326060523/figure/fig1/AS:642817676304388@1530271085420/Example-of-a-Network-to-Network-In-Figure-3-there-is-two-network-connected-to-a-router.png)
- You can then connect this network to the internet, or, **ideally** to a firewall that connects to the internet
- Firewalls can also be used for other networks, or as software to protect the devices on the network

## Identifying and blocking intrusions/attacks
Security devices: 
- **Firewall**
- Honeypots
- **IDS intrusion detection system**
- **IPS intrusion prevention system**
- Snort??

## Internal server
- Active directory -> Set of protocols/services for microsoft that controls the network. Used to authenticate the user in a windows server
- VoIP -> Phone calls (uses IP)

## Access Control
- Authorization 
- Authentication
- Authoring (Logging)

## Performance
- Load balancers are used to manage multiple connections. It's better to use them if there's too much traffic that might slow down traffic to the server, sinc eit resdistributes network traffic across multiple servers.
- Proxys: Acts as a Middleman for Inbound/Outbound Internet Traffic

## Example of Proxy
Let's say that you're in the middle of Ecuador and you have a bunch of routers sprawled out across the country. Now let's say one of those routers are connected to a proxy server, which is a server that keeps track of a cache  that knows where to transmit packets to the other routers in the country. This caching can be used to speed up requests, so that you can send packets more quickly. In Ecuador, apparently there was 1/2 Gpbs of internet to be shared by everyone in the country. This is occured in the early stages of the Internet, network infrastructure is probably different now.

## NGIX + Security
- NGIX was commonly used for webservers
- Opening a port with a proxy server is generally dangerous, and people can listen in onto connections
- You can put a firewall, use security devices, etc. to prevent this sort of issue from happening

## Firewalls and layers on the OSI model
- Firewalls can operate on multiple layers. They most often operate on layer 3 (IP) and 4 (TCP/IP) of the OSI model.
- Some even work on layer 7 and do more advanced tasks.
- Popular types of firewalls:
  - Stateful Packet Inspection (SPI): "A SPI firewall is stateful because it understands the different states of the TCP (transmission control protocol) protocol. It knows what is coming and what it going and keeps track of it all. " [(Source)](https://petri.com/csc_routers_switches_and_firewalls/)
