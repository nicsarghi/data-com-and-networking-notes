---
layout: post
title: Lecture 13
date: 2022/10/26
file: DataComm-Lecture13.pdf
dir: /lectures/
--- 
# Transport Layer Security (TLS) 
```
Transport Layer Security, or TLS, is a widely adopted security protocol designed to facilitate privacy and data security for communications over the Internet. A primary use case of TLS is encrypting the communication between web applications and servers, such as web browsers loading a website.
```
[(Via cloudflare)](https://www.cloudflare.com/learning/ssl/transport-layer-security-tls/)

In TLS, the communication is first established. After it is established, the data that is sent between the two communicating nodes is encrypted.

## **Key Concepts**
- TLS Handshake
- Session caching
- Stateless resumption mechanism
- Chain of trust
- TLS cipher suites

## Learning Objectives
- Acquire knowledge of TLS protocol
- Learn to write Java code that allows
data to be transferred securely using
TLS

## The difference between TLS and SSL
- TLS evolved from a previous encryption protocol called Secure Sockets Layer (SSL), which was developed by Netscape. It was to enable ecommerce transaction security on the Web.
- It was going to be referred to as a newer version of SSL, but they ended up changing the name so that it's no longer associated with Netscape  [(Source)](https://www.cloudflare.com/learning/ssl/transport-layer-security-tls/)
- When the SSL protocol was standardized by the IETF, it was renamed to Transport Layer Security (TLS)
- TLS was designed to operate on top of a reliable transport protocol such as TCP
    - However, there is also the Datagram Transport Layer Layer Security (DTLS) protocol which is based on TLS

## How TLS ensures security
- It's designed to provide three essential services to all applications running above it:
    - Encryption
    - Authentication
    - Data integrity
- In order to establish a cryptographically secure data channel, the connection peers must agree on establishing a cipher suite and the keys used for the cipher suite. This is so that they can both encrypt the data during communication.
- The TLS handshake protocol is responsible for performing this exchange

## TLS Handshake
- During TLS handshake phase, the communicating peers authenticate their identity
- After the TCP handshake, there will be a handshake for the key exchange so that messages are encrypted
- When used in the browser, this authentication mechanism allows the client to verify that the server is who it claims to be
    - This verification is based on the established chain of trust
- The server can also optionally verify the identity of the client
- The communicating peers use public key cryptography to negotiate a share secret key:
    - This is done without having to establish prior knowledge of each other
    - The negotiation is done over a secure channel
- Before the client and the server can begin exchanging application data over TLS, the encrypted tunnel must be negotiated
- The communicating peers must agree on the following:
    - The version of the TLS protocol to use
    - The cipher suite that will be used to provide confidentiality, integrity and authentication
    - The server certificate and optionally, the client certificate must also be verified  
<br>
![TLS handshake](images/tls-handshake.png)

## Diffie-Hellman Key exchange
- It allows the client and server to negotiate a shared secret without explicity communicating it in the handshake
- The server's private key is used to sign and verify the handshake
- The establushed symmetric key never leaves the client or server **->** It cannot be intercepted by a passive attacker even if they have access to the private key

### Interesting tidbit
- Back then, the client and the server would use an RSA key to communicate encrypted messages with eachother. Both have a public key that they first exchange in the TLS communication. 

## [Diffie-Hellman video](https://www.youtube.com/watch?v=NmM9HA2MQGI)
- This video uses colored dyes to explain how the diffie-hellman key exchange works
- Make sure to rewatch it when you can, it does a good job of explaining why it's difficult to decyper two keys being exchanged and how it works without getting too deep into the actual maths for it

## TLS Session Resumption 
- The extra latency and computational costs of the full TLS handshake impose a serious performance penalty on all applications that require secure communication
- To help mitigate some of the costs, TLS provides a mechanism to resume or share the same negotiated secret key data between multiple connections

## Session identifiers
- Server creates and sends session ID of size 1 – 32 bytes to client as part of the ServerHello message during the full TLS negotiation
- With the session ID in place, both the client and server can store the previously negotiated session parameters (keyed by session ID) and reuse them for a subsequent session
- The client can include the session ID in the ClientHello message to indicate to the server that it still remembers the negotiated cipher suite and keys from previous handshake and is able to reuse them
- **Basically: During the TLS negotiation, a server makes a session ID and sends it to the client. The session ID is kept between them for the entire time they're communicating.**
- A limitation of the session identifier is that the server needs to create and maintain session cache for every client

## Session Tickets
- Removes the requirement for the server to keep per-client
session state
- Instead, if the client indicates that it supports session
tickets
- The server can include a New Session Ticket record,
which includes all the negotiated session data encrypted
with a secret key known only by the server
- This session ticket is then stored by the client and can be included in the SessionTicket extension within the ClientHello message of a subsequent session
- Thus, all session data is stored only on the client, but the ticket is still safe because it is encrypted with a key known only by the server

## Session caching
- The session identifiers and session ticket mechanism are respectively commonly referred to as **session caching** and **stateless resumption mechanisms**
- These mechanisms made it possible for the abbreviated TLS handshake to take place, rather that the full TLS handshake, which involves 2 additional messages

## Chain of Trust and Certificate Authorities
- **Authentication is an integral part of establishing every TLS connection**
    - Both Alice and Bob generate their own public and private
    keys
    - Both Alice and Bob hide their respective private keys.
    - Alice shares her public key with Bob, and Bob shares his
    with Alice
    - Alice generates a new message for Bob and signs it with
    her private key
    - Bob uses Alice’s public key to verify the provided message
    signature
- **The question is, how can one be certain that the public belongs Alice or Bob?** A trusted 3rd party such as Charlie can vouch for the authenticity of Bob’s public.

## Certificate Authorities
- A trusted third party that is trusted by both the subject (owner) of the certificate and the party relying upon the certificate
- Every operating system and most browsers are shipped with a list of well-known certificate authorities
- Thus, we trust the vendors of these software to provide and maintain a list of trusted parties
- Thus, we trust the vendors of these software to provide and maintain a list of trusted parties

## Certificate Revocation
- Occasionally the issuer of a certificate will need to revoke or invalidate the certificate due to numerous of possible reasons:
    - The private key of the certificate has been compromised
    - The certificate authority itself has been compromised
    - Other reasons

## CRL
- Revoked certificates can be placed on CRL
    - Certificate Revocation List (CRL) is defined by RFC 5280 and specifies a simple mechanism to check the status of every certificate
    - Each certificate authority maintains and periodically publishes a list of revoked certificate serial numbers
    - Anyone attempting to verify a certificate is then able to download the revocation list, cache it, and check the presence of a particular serial number within it
- CRL has the following limitations:
    - CRL list can be quite long, and each client must retrieve the entire list of serial numbers
    - No mechanism for instant notification
    - CRL fetch may fail due to numerous reasons
- To address some of the limitations of the CRL mechanism, OCSP was introduced by RFC 2560, which provides a mechanism to perform a     real-time check for status of the certificate
    - Unlike the CRL file, which contains all the revoked serial numbers,
    - OCSP allows the client to query the CA’s certificate database directly for just the serial number in question while validating the certificate chain

## TLS Record Protocol
- As is the case for protocols in the TCP/IP protocol stack, such as TCP and IP, that operates below TLS, all data exchanged within a TLS session is also framed using a well-defined protocol
    - This protocol is called TLS Record protocol
    - TLS Record protocol is responsible for identifying
    different types of messages (handshake, alert, or
    data via the "Content Type" field), as well as
    securing and verifying the integrity of each message  
<br>
![TLS record structure](images/tls-data.png)

## Best practices of implementing TLS
- Versions of TLS prior to 1.2 should not be used because they are considered insecure ( You can specify the version in the TLS listener)
- TLS  can only be as secure as the used cipher suites. It is best to limit your TLS security enabled application. Use cipher suites that are **known to provide optimal security**, which includes the following:
    - TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
    - TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256
    - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
    - TLS_RSA_WITH_AES_128_GCM_SHA256
- [Here's a list provided by Oracle that you can use for implementing cipher suites in Java: https://docs.oracle.com/javase/8/docs/technotes/guides/security/SunProviders.html](https://docs.oracle.com/javase/8/docs/technotes/guides/security/SunProviders.html)
- Cipher suites can be configured using TLS listeners, which are set up using xml files

## HiveMQ can use TLS for security
- [Here's an article that allows you to use TLS encrypted communications for your MQTT broker.](https://www.hivemq.com/blog/end-to-end-encryption-in-the-cloud/)
- Feel free to use it for your project. 
