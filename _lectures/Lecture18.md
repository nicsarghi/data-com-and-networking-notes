---
layout: post
title: Lecture 18
date: 2022/11/16
file: DataComm-Lecture18.pdf
dir: /lectures/
--- 

## Reminder: Noise
- Signals are represented by an electrical signal (voltage), but realistically there's always going to be noise. Because there's some slight jitter in the voltage.
- Transmissions can also contain errors
- Could be caused by problems in the cabling. Like a rat eating away the wires.

## Crosstalk
- Crosstalk defines the unwanted coupling between two different
signal paths
  - For example, hearing another conversation
while talking on the telephone
- Relatively constant and can be reduced with
proper measures  
<br>
![Crosstalk](images/crosstalk.png)

## Echo
- End of cable ends up echoing signals to the front of the signal  
<br>
![Echo](images/echo.png)

## Jitter
- Basically since frequencies can vary, they end up not appearing as perfect squares, but instead as a signal thats slowly increasing...
- It's a result of small timing irregularities during the transmission of digital signals. It typically occurs when a digital signal is repeated
over and over
- If serious enough, jitter forces systems to
slow down their transmission
![Jitter](images/jitter.png)

## Other categories of noise
- **Delay Distortion:** Occurs because the velocity of propagation of a
signal through a medium varies with the
frequency of the signal
- **Attenuation:** The continuous loss of a signal’s strength as it
travels through a medium

# Parity checks
## Simple parity
  - Parily only detects odd numbers of bits in error. You can add a parity bit at the end of the data, and it will check if the number of 1s is an even or odd value
- What happens if the character 10010101 is send and the first two zeroes accidentally become two ones? (11110101)
    - Will there by a parity error?

## Longitudinal parity
- Adds a parity bit to each character then adds a
row of parity bits after a block of characters
- The row of parity bits is actually a parity bit for
each “column” of characters
- The row of parity bits plus the column parity bits
add a great amount of redundancy to a block of
characters.
- **TLDR:** Each column of the final row represents whether or not the parity bits in the column is even/odd
- It's basically a parity matrix.  
<br>
![Parity matrix](images/parity-matrix.png)
- ...But even this parity matrix can have its own issues...
![Parity matrix flaw](images/parity_issues.png)
- As such, both simple parity and longitudinal parity do
not catch all errors
- Simple parity only catches odd numbers of bit
errors
- Longitudinal parity is better at catching errors
but requires too many check bits added to a
block of data
- **A better method to check for data is the arithmetic checksum.**

## Arithmetic Checksum
- Used in TCP and IP on the Internet
- Characters to be transmitted are converted
to numeric form and summed
- Sum is placed in some form at the end of
the transmission

```
Simplified example with a collection of values:
+ 56
+ 72
+ 34
+ 48
______
 210

Bring 2 down:
210 -> 10 and 2

Add to right-most position:
+ 10
+ 2
_____
  12

```
- Receiver performs same conversion and
summing and compares new sum with sent
sum
- TCP and IP processes a little more complex
but idea is the same
- But even arithmetic checksum can let errors
slip through... So is there something better?

## Cyclic Redundancy Checksum
- CRC error detection method treats the
packet of data to be transmitted as a large
polynomial
- Transmitter takes the message polynomial
and using polynomial arithmetic, divides it
by a given generating polynomial
- Quotient is discarded but the remainder is
“attached” to the end of the message
- The message (with the remainder) is
transmitted to the receiver
- The receiver divides the message and remainder
by the same generating polynomial
- If a remainder not equal to zero results, there
was an error during transmission
- If a remainder of zero results, there was no
error during transmission

### CRC polynomials  
![Polynomials](images/src-polynomials.png)

### Performance on error detection
![ECC detection performance](images/error-percentages.png)

## Reminder: Data Transmission 
- Simplex: Only really used for some monitors (screens). It's a unidirectional communication.
- Half-duplex: Send and receive, *but **not** at once*
- Full-duplex: Send and receieve, *can be at once*

## Reminder: Compression
- Lossy format: Some of the data is lost 
    - JPG format for pictures, it usually is inperceptible or barely visible. 
    - MP3 music is also another example
- Lossless: The data is overall the same, no information is lost 
(PNG format for pictures)
    - If you want to compress a financial file, you'll likely want it to be lossless (important data)

# Reminder: Encoding schemes
## ASCII
- 7 bit-value
## Unicode 
- Represented the following way: U+UFFFF
- ASCII characters can easily be transferred to unicode characters:
    - 0x48 (The hex value for the character 'H') will be converted to U+0048
- **Contains two encodings:**
    - UTF-8: 48 bits
    - UTF-16: 48 bits + a 16 bit plane

## UTF-16
- BOM: Byte Order mark
- BE: Big Endian
- LE: Little Endian

## UTF-8
- Byte order mark is EF, BB, BF

## Keystore tutorial
- Ponce suggested using this tutorial for the keystores: https://jenkov.com/tutorials/java-cryptography/keystore.html
- It provides a basic guide for storing Certificates and Private Keys to the keystore. You can also store secret keys (symmetric key)