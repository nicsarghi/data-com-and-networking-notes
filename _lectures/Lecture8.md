---
layout: post
title: Lecture 8
date: 2022/09/22
file: DataComm-Lecture8.pdf
dir: /lectures/
--- 

# Quiz Practice 1
We really didn't do much here except [watch the video on modulation](https://www.youtube.com/watch?v=Iyzpt3bKTTI) and do a practice quiz in class.
You don't have to memorize the terms in modulation, but it's a good idea to understand what it does. 

1. Which of the following term is defined as the absolute value of the difference between the lowest and highest?
**Answer: Bandwith** 
 
2. Which of the following network connection "boxes" is used to determine which path a packet will take to get to its destination on the internet?
**Answer: Router**

3. Which of the following topologies is the most used in Local Area Networks?
**Answer: Star**

4. An IP address consists of two parts, a network portion, and a host portion. What is used to determine how big each part is?
**Answer: Subnet mask**

5. Which of the following part list items is not required when building a Peer-to-Peer network with 3 computers?
**Answer: 1x Server**

6. Consider this scenario: A user subscribes to an application and runs that application in the cloud
**Answer: SaaS**

7. 
What Network are you on? 
**Answer: 192.168.211.0, a class C address**

What is the maximum number of computers that can be connected to that network? 
**Answer: In theory 256, but 254 (since 255 and 0 are reserved)**

How does your computer access the internet? 
**Through the gateway node on the network (192.168.251.1)**

8. 
What is a GPIO?  
**Answer: General Purpose Input Output pins aka GPIO pins are situated on the raspberry pi, and are used for supplying or receiving electrical signals from it.** 

The purpose of 2 GPIO pins:  
**Answer: 3V3 and 5V volts supply 3 volts and 5 volts of power respectively to a circuit. Ground receives energy from a circuit and connects to the ground. There's also I2C and SPI which are used to**