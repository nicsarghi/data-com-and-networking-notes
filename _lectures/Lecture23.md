---
layout: post
title: Lecture 23
date: 2022/11/29
file: DataComm-Lecture23.pdf
dir: /lectures/
--- 

# WANS and the evolution of networks

## Metro Ethernet Network
- An Ethernet transport network that
provides point-to-point or multipoint
connectivity services over a metropolitan
area network (MAN)
  - Ethernet originated as a LAN technology, and
became a replacement for low-speed WAN
technologies
- Business, residential, and mobile
subscribers select Ethernet services from
service providers because of its cost
effectiveness, flexibility, and simplicity
- Service providers use Metro Ethernet for:
  - Interconnecting business offices or data
centers
  - Connecting residential subscribers or
businesses to the Internet
  - Providing connectivity to public or private
cloud data centers

## LANS
- But what happens if you want to connect two LANS?
## Virtual LANS
- Usually switches with specialized tables. The table identifies each virtual LAN by associating them to specific ports
- An OS manages this virtual network, and a small computer can configure it
- H devices (Machine Learning)
- Virtual Layers create the packet faster to transmit data more efficiently

## WAN (Wide area network )
- Connectivity between multiple locations via
the Internet

- Software-defined WAN (SD-WAN)
- Enables customer to control and
configure WAN using point-and-click
interface
- Requires physical connection to be
preinstalled

## WAN and SD-WAN Business Drivers
- Need to support cloud
migration
- Need to enhance
network security
- Must support network
agility
- Must have network
visibility

## WAN Technologies in the 1970s
- Modem
  - A device that transmits and receives signals via tones
  - Modulation and demodulation is a technique for transmitting 1’s and 0’s
over an analog transmission line
- Modem transmission speeds peaked at 56 Kbps when analog dial-up
services were deployed for initial Internet access
- Analog leased lines or circuits
  - Provide a point-to-point transmission path
  - Connection is always on
- Circuit-switched networks
  - Public Switched Telephone Network (PSTN)
  - Integrated Services Digital Network (ISDN)
- Packet switches
  - Derives the best path to take within the packet-switched network
to reach the destination via virtual circuits
  - Digital transmission switches
  - Deployed by service providers
  - Replaced analog transmission switches
  - Supports the DS0/DS1/DS3 family of digital communications
- Satellite communications
  - Provides the equivalent of a leased line

## WAN in 1990s
- Use of optical fiber and SONET backbone networks
-Asynchronous Transfer Mode (ATM)
  - Solution for high-speed transport services for voice, video, and data
transmissions
  - Switches establish point-to-point connections between endpoints to
support data transmission

## WAN in the 2010s
Software-Defined Networking (SDN):
- A network in which the control plane is physically
separate from the forwarding plane, and a single
control plane controls several forwarding devices
- Its an emerging architecture that is dynamic,
adoptable and cost-effective
- This architecture separates the network control and
forwarding functions; thus, enabling the network
control to become programmable
- See https://opennetworking.org/sdn-definition

### SDN
SDN is widely deployed by cloud provides;
other use cases involved the following:
- Network Virtualization is the first widely-
adapted use case of SDN
  - Example of network virtualization include VPN
(Virtual Private Network) and VLAN (Virtual Local
Area Network)

### SDN use cases:
Software-Defined Wide Area Network (SD-
WAN) is another use case of SDN
- Enterprises have for many years been buying WAN
services from telecommunications companies to
obtain reliable and private network services to
interconnect there many locations
  - For most of the 21st century, MPLS networks were the
choice for interconnecting enterprises’ locations
- SD-WAN has become a popular alternative to MPLS
SD-WAN is easer to provision and
controlled, compared to MPLS networks
  - For example, a SD-WAN controller can receive
policies centrally and pushes them out to edge
switches at various sites
  - The switches build an overlay of tunnels over the
Internet or other physical networks, and
implement policies including allowing direct
access to cloud services

## WAN Management + Requirements

### Fault Management
- Addresses a down or non- functioning
link, circuit, or network connection
### Requirements
- Fault monitoring 
- Incident response
- Operations
- Resiliency
- Redundancy
- Monthly reporting

### Configuration Management
- Pertains to software changes to the WAN and
network devices (routers, switches, etc.)
### Requirements
- Configuration
identification
- Configuration
auditing
- Change control
- Provisioning
speed 
- Ticketing
- Version control
- Monthly
reporting

### Accounting management
- Refers to the ongoing management of third-party
vendors and telecom service providers
### Requirements
- Accounting
- Asset inventory
and
management
(WAN)
- Audit of service
provider billing
- Performance
monitoring and
reporting
- Service level
agreements
(SLAs)
- Vendor
management
- Monthly
reporting

### Performance management
- Deals with
optimization,
effectiveness,
and efficiency
of the WAN
and its
overall
performance
### Requirements
- Bandwidth
- Capacity
planning
- Distance (local,
metro, long-
haul)
- Monitoring
- Throughput
- Traffic shaping
- Visibility
- Monthly
reporting

### Security management
- Deals with
ongoing
security
operations
and
management
functions
### Requirements
![Security Management Requirements](images/security_management.png)

## WAN Solutions for the Enterprise
- High-speed Internet access (scalable
business-grade Internet access)
- Secure network connections (behind
an IP stateful firewall)
- NAT with private IP addressing
(10.x.x.x /16)
- VLAN capability for departmental
LANs
- Financial analysis for WAN bandwidth
versus cost
- Dynamic Multipoint Virtual Private
Network (DMVPN)
-  Bandwidth rate limiting and threshold
setting for Internet
- SD-WAN to control and manage a
hybrid WAN environment
- Wireless access with WPA2-Enterprise
for mobile devices throughout the
enterprise environment
- IP stateful firewall (ideally with an
IDS/IPS)
- Remote access with VPN and
multifactor authentication
- Web content filter (URL monitor and
filter)

## WAN Solutions for the Small to Medium Business (SMB)
- High-speed Internet access
(scalable business-grade
Internet access)
- Secure network connections
(behind an IP stateful firewall)
- NAT with private IP addressing
(172.16.x.x /12)
- VLAN segmentation to logically
separate departmental LANs
and traffic
- Public-facing DMZ/VLAN
- Extension of Layer 2 networks
- Connectivity to data centers or
cloud hosting facilities
- Point-to-Point Protocol (PPP)
- Remote access with VPN and
multifactor authentication
- Web content filter (URL
monitor and filter)

## WAN Solutions for the Small Office/Home Office (SOHO)
- High-speed Internet access
(scalable business-grade
Internet access)
- Secure network connection
(behind an IP stateful firewall)
- NAT with private IP addressing
(192.168.x.x /8)
- VLAN capability to logically
segment office/work LAN from
guest/family LAN
- Bandwidth rate limiting and
threshold setting for
guest/family LAN to Internet
- Wireless access with WPA2-
Enterprise for mobile devices
throughout the home
- IP stateful firewall (ideally with
an IDS/IPS)
- Remote access with VPN and
multifactor authentication
- Web content filter (URL
monitor and filter)


## MPLS (Multiprotocol Label Switching)
- MPLS is a packet-switching technology that operates at Layer
2; is independent of routing tables or any routing protocol
- A method for engineering traffic patterns by assigning short
labels to network packet that describe how to forward them
through the network
- Supports the creation of Virtual Private Networks (VPN)
- Service providers adopted MPLS switches for use as enterprise
WAN solutions
- Allows IP traffic to flow in a full or partial mesh configuration
- Supports Quality of Service (QoS) and Class of Service (CoS)

## MPLS Connectivity
- Virtual Routing Forwarding
- Forward packets to other routers in a network

## MPLS networks video
### Private Lines: 
- Complicated to manage and costly as you expand

### Internet:
- Goes everywhere and cheap to access
- Less secure and is poorly performs. Since its easily access by everyone
- You have to create a private tunnel taking you to the internet, think of SSL or VPNs
- No traffic has priority over anything else, so it's generally slower. 
### Label Switching:
- Switches in the middle usually handle transferring packets between LAN
- Easier to manage packets without going to upper layers. Labels are attributed to the Frames and send them to other switches without reading too deeply into the packet
- Label Switching doesnt depend on IP
- Operated by major telecom networks w fiber optics, gets rid of congestion and latency more
- It can also be attributed to a priority class
- Costs more than the Internet obviously. Still better than dedicated private lines

# Software defined networking video 1
- Traditional networking -> Combines hardware and software. 
- Smart controllers can define security policies for each local networks
- SDN works with wide area networks to connect to branch offices (SD-WAN)
- Microsegmentation: Can present public-facing ips and ultra-secure tunnels
- Network Function Virtualization: Replacing hardware like load balances with software on the servers
- Manages deluge of traffic from IoT
- Applied to many different areas of networking
- TLDR: Instead of installing routers and switches manually, server machinery just handles all of it.

# SD-Wan
- Trad MPLS: A branch connects to the head office, then sends a packets to the internet.
- SDN-Wan: Services are mainly connected to the cloud and it's easier to connect to. Generally will be cheaper in the future. It's common today

# Label Switching
Routing in IP networks
- In a traditional IP network, packets are
transmitted with an IP header that includes a
source and destination address
- When a router receives such a packet, it
examines its forwarding tables for the next-hop
address associated with the packet's destination
address and forwards the packet to the next-hop
location

In an MPLS network, each packet is
encapsulated with an MPLS header
- When a router receives the packet, it copies the
header as an index into a separate MPLS
forwarding table
- The MPLS forwarding table consists of pairs of
inbound interfaces and path information
- Each pair includes forwarding information that the
router uses to forward the traffic and modify, when
necessary, the MPLS header
n contrast, MPLS routers within an AS
determine paths through a network through the
exchange of MPLS traffic engineering
information
- Using these paths, the routers direct traffic
through the network along an established route
- Rather than selecting the next hop along the
path as in IP routing, each router is responsible
for forwarding the packet to a predetermined
next-hop address

