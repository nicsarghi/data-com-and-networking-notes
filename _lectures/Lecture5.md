---
layout: post
title: Lecture 5
date: 2022/09/8
file: DataComm-Lecture5.pdf
dir: /lectures/
---  

# Reminders
- In tests you might be given the header of a protocol data init (plus additional information) and you might need to identify it. 
- Remember that an ethernet frame contains a header, data and trailer. The trailer contains the checksum and that checks if the frame is good ( extra metadata)
- **Remember that the midterm is on the 29th of September (Thursday)**

## IPv4 address classes
- A -> netmask of [ 0-127 ].0.0.0
- B -> netmask of [ 128-191 ].[ 0-255 ].0.0
- C -> [ 192-223 ].[ 0-255 ]-[ 0-255 ].0

- Since there's less possible hosts on a class C network, it's usually used smaller networks.
- The network "segment" of an ip will be the same for all hosts on a network.
- Since it's the standard to use these classes, you should use these ip ranges. It makes network administration easier. Furthermore, most private ip ranges are reserved and not claimed by public ips.

## Network Access Layer: TCP/IP layer
- Operates at the bottom layer of the TCP/IP reference model
- Reliability is ensured by checksums, which can make it so that the transmitted data is sent
- Integrity enrefers to whether the binary data made to to the destination intact and accurate
- Trasmission mediums can consist of air (Radio frequency), metal (copper, unshielded twisted pair, shielded twisted-pair or coaxial cable), or glass (optical fiber)

## IEEE standard 802.3 AKA the rules of communication for ethernet (fiber optics and copper )
- Ensures collision detection 
- Checksum and Correction 
- Standard for the physical layer and data link layer's media acces control of wired ethernet
- Earlier versions of the standard 802.3 defined CSMD/CD
- Tranceivers in a full duplex will have two systems for sending and receiving data. There is no collision in that system. CSMA/CD is not needed in full duplex connections.
- Still supported for backwards compatibility
- Switches can be full duplex or half duplex depending on configuration.

## IEEE standard 802.3 for hubs
- Defines which connections should send data at what times to avoid collisions
- CSMA/CD (Carrier sense multiple access with collision detection)
- In switches, the collision domain is the path from one node to another, which is where a collision may occur.
- Machines that are not able to be full duplex will connect into a half-duplex system, so the switch needs to know how to handle those channels while avoiding collisions.

## Data link division
- A packet is divided into parts of 45 and 1500 bytes.
- In cases where the packet is less than 45 bytes, it'll be padded with extra bytes.
- This occurs in a packet switched network where each packet is transmitted through the nodes in the network.

![Data link frame](images/ethernet-header.png)

## In protocols, the sizes for data will always be the same, because they are standardized

## Bridging
- Internetworking always requires a bridge, so that two networks are able to communicate together. 
- There is a MAC layer bridge between these two networks. This bridge makes a filter or forward decision based on the MAC address forwarding table.
![MAC bridge](images/bridge-forwarding-table.png)

# Switch Forwarding Methods
- When there's a switch, you might want to optimize the performance
- CISCO switches can contain both for example, but you might want to configure it

![Switch forwarding](images/switch-forwarding.png)

## Cut-through switching
- Receives a frame from a node and immediately sends it "at the moment". Does not examine the frame
- Fast method, increseases network throughput and performance
- Does not check for errors
- Depends on the use cases for sending packets

## Store-and-forward switching
- LAN has to receive the entire Ethernet frame and make a forwarding decision based on the MAC address table
- Allows the switch to check the checksum of the frame before it can forward it to the next frame, since it drops packets with errors prior to forwarding them
- Uses less of a network's capacity but increases latency

# Layer 2 vs Layer 3 switches
Depends on what level the switch operates

## Layer 2 switches
- Operate at the Data Link Layer
- Examines the MAC alayer addresses of Ethernet frames

## Layer 3 switches
- Can determine which route to take for an incoming packet
- Operates at either the Data aLink Layer or Network Layer
- Software that lets them function like layer 2 switches or multiport bridges
- Examines network layer address within the ethernet frame
- Look up destination IP network number in thieir IP routing tables and make a path determination decision

# Physical layer
## Transmission Media
### Copper
- Shielded twisted-pair (STP), (metal cover covering the cable to avoid induction )
- Unshielded twisted-pair (UTP). Common in office buildings and wired network environments.

### Coaxial
- Round and with a metal top. The copper is one line.
- Rugged indoor or outdoor cable.

### Glass aka Fiber optics
- Glass core surrounded by a glass cladding. Light travels through the core.
- Fiber optics can support large amounts of data transmissions over very
far distances; often used for long-haul data transmissions. 

## Tables from the slides about ethernet speeds
**Know the IEEE standard names for the exam**
![Cable speeds and types](images/cable-types.png)
![Ethenet standard release dates](images/ethernet-standard-dates.png)
![Bandwith speed evolution](images/ethernet-bandwith-evolution.png)
![GigE and 10 GigE Specifications](images/gige-specs.png)
- **Not noted: RJ-45 refers to twisted pair copper** 

# Signals
- Technically exists everywhere and anytime since it represents how we communicate (aka a medium)
- Signals can be represented by the electric or electromagnetic impulses used to encode and transmit data
- Networks and data/voice comm systems transmit signals
- Signals can be analog or digital

## Analog signals
- Continuous waveform, usually consists of music, voice, etc. You can represent an analog signal best with sound.
- Harder to separate noise from an analog signal than it is from a digital signal (Noise is unwanted electrical or electromagnetic energy
that degrades the quality of signals)
- Looks more like a sine wave function
![Analog signal wave](images/analog-signal-wavw.png)

## Digital signals
- Discrete or non-continuous waveform.
- Looks more like a square function, and appears only in a fixed number of forms.
- Noise affects your ability to discern a high voltage from a low voltage. If there's too much, it's impossible to tell.
- 5V is commonly used for digital signals. They also tend to be consistent, so the waveform is square-like
![Digital signal wave](images/digital-signal_wave.png)
