---
layout: post
title: Lecture 12
date: 2022/10/20
file: DataComm-Lecture12.pdf
dir: /lectures/
--- 

# Security Video [(Combatting Hardware Trojans)](https://www.youtube.com/watch?v=OYuggCgruuA&feature=youtu.be)
- Ponce posted a link to a video (it's on Moodle, but the link is provided here) that explains network security from the hardware aspect.
- Covers hardware security, how likely you are to be hacked and security problems in hardware.
- Hardware trojans
- Intellectual property theft can be an issue; people steal blueprints that can be examined and sold to competitors. Blueprints can also be obfuscated. A chips functionality is also modified by a hardware key. Without the proper key, it will outptut garbage data. 
- The designer is the only person who can unlock the chip with the key after fabrication.
-  USBs that sign data with keys will usually have processors on them to ensure security (?)

# Network Architecture
- **Network architecture refers to the way network devices and services are structured to serve the connectivity needs of client devices** [(Via Cisco)](https://www.cisco.com/c/en/us/solutions/enterprise-networks/what-is-network-architecture.html)
- We commonly know of two network architectures:
    - Client/Server
    - Peer-to-Peer
- **In distributed computing**, we usually combine p2p and client/server architectures at the software level.
- **A distributed system is a collection of independent components located on different machines that share messages with each other in order to achieve common goals.** [(Via confluent)](https://www.confluent.io/learn/distributed-systems/)

## Architectures can also be centralized or decentralized
### **Centralized architecture:** 
A centralized architecture implies the availability of a single or a few entities that have control over the entire network [(Source)](https://www.sciencedirect.com/topics/engineering/centralized-architecture)
    - Examples are Edge computing and proxies
### **Decentralized architecture:** 
A decentralized network architecture distributes workloads among several machines, instead of relying on a single central server. [(Source)](https://www.n-able.com/blog/centralized-vs-decentralized-network)
    - Examples are Peer-to-peer networks such as Tor

# TCP/IP protocols
- We're used to using HTTP over TCP/IP, but there's a bunch of other protocols that can be used for other functions

## MODBUS TCP
- Basically a protocol for transmitting messages over TCP/IP. Used in distributed systems
```
"Modbus TCP/IP (sometimes referred to as the Modbus TCP protocol or just Modbus TCP) is a variant of the Modbus family of simple, vendor-neutral communication protocols intended for supervision and control of automation equipment."
```
[(Via rtautomation)](https://www.rtautomation.com/technologies/modbus-tcpip/)

## REST or CoAP
- Also commonly used in IoT
- The CoAP model is similar to REST (CoAP describes it as a REST Model):  
    ```
    The Constrained Application Protocol (CoAP) is a specialized web transfer protocol for use with constrained nodes and constrained networks in the Internet of Things.
    The protocol is designed for machine-to-machine (M2M) applications such as smart energy and building automation.
    ```
[(Via coap.technology)](http://coap.technology/)

## Publish-subscribe model
- *Not* an architecture, but used in many service architecture. 
- **Publish/subscribe messaging, or pub/sub messaging, is a form of asynchronous service-to-service communication used in serverless and microservices architectures.** [(Via amazon.aws)](https://aws.amazon.com/pub-sub-messaging/) 
- Ponce described it as an event-based solution.
- We'll be using this model for the project, by sending data such as time, temperate, humidity to the GPIO pins and then publishing it to an MQTT broker.

## Code Mobility
```
" In distributed computing, code mobility is the ability for running programs, code or objects to be migrated (or moved) from one machine or application to another. This is the process of moving mobile code across the nodes of a network as opposed to distributed computation where the data is moved. It is common practice in distributed systems to require the movement of code or processes between parts of the system, instead of data. "
``` 
[(Via DBpedia)](https://dbpedia.org/page/Code_mobility)  
<br>
- It's basically the concept of migrating code over a network, either from one machine/app to another one, so that it may eventually get executed. 
- Think of Javascript files that get transmitted to the client from the server. Flash animations are another good example 

### Examples
- **Apache Kafka:** Apache Kafka is a distributed data streaming platform that can publish, subscribe to, store, and process streams of records in real time. It is designed to handle data streams from multiple sources and deliver them to multiple consumers. In short, it moves massive amounts of data—not just from point A to B, but from points A to Z and anywhere else you need, all at the same time. [(Via redhat)](https://www.redhat.com/en/topics/integration/what-is-apache-kafka)  
<br>
![Apache kafka architecture](https://daxg39y63pxwu.cloudfront.net/images/blog/apache-kafka-architecture-/image_7224627121625733881346.png)
- **

# MQTT
- MQTT is MQ Telemetry Transport (According to ponce, MQ is Message Queuing)
- A machine to machine network protocol that performs message queueing.
- It's an open OASIS standard and an ISO recommendation (ISO/IEC 20922). [You can actually download and view the information for the standard here.](https://www.iso.org/standard/69466.html) 
- Most of the MQTT specs are provided on the slides and on Moodle. Note that the following information is going to rely heavily on the powerpoints, but if you want to view the documentation for MQTT, then [I highly recommend reviewing MQTT essentials, as this is likely what Carlton based his slides on.](https://www.hivemq.com/mqtt-essentials/)

[Via aws.amazon.com: ](https://aws.amazon.com/message-queue/)
```
A message queue is a form of asynchronous service-to-service communication used in serverless and microservices architectures. Messages are stored on the queue until they are processed and deleted.
```

## Learning Objectives
- Acquire knowledge of MQTT
protocol (**For the exam!**)
- Learn to write Java code that
sets up connection between
MQTT clients and broker, and
subscribe to and publish
messages (**For the project!**)

## Key Concepts
- Publish-Subscribe pattern
- MQTT broker
- MQTT topics
- $-symbol topics
- MQTT wildcard
- Last Will messages
- MQTT Message formats

## What is MQTT (MQ Telemetry Transport)
- **The most used messaging protocol for the Internet of Things (IoT)**
- "MQ" -> MQ Series; it's a product developed by IBM to support MQ telemetry transport
- **It is a set of rules that defines how IoT devices can publish or subscribe to data over the Internet**
- **It is used for messaging and data exchange between IoT and Industrial IoT (IIoT) devices**
- IOT is distributed and has more devices in it

## Public-subscribe model of MQTT
- Consists of the **producer, consumer and broker.**
- It relies on the publish and subscribe model, it's not an architecture itself but it's a pattern.
- The sender (publisher) and the receiver (subscriber) communicate via Topics.
- They do not interact with eachother, as they have no awareness of the existence of eachother.

## The broker
- The broker is the 3rd party agent. It's servers to be the middleman between the publisher and subscriber.
- It filters all incoming messages and distributes them correctly to the Subscribers
- The filtering activity of the broker makes it possible to control which client/subscriber receives *which* message

## Publish-subscribe vs Client-server
- MQTT publish/subscribe pattern (also referred to as pub/sub) provides an alternative to traditional client-server architecture
- In the client-sever model, a client communicates
directly with an endpoint, referred to as a server
- The pub/sub model decouples publishers from subscribers. The clients that send messages and the clients that receive messages  are independant from eachother.
- This is because the publishers and subscribers never contact eachother directly, and are not aware of eachother's existence. It's up to the broker to be the middleman between them to these messages.
- **This means that all publishing and subscribing go through the broker!**  

![Publish-subscribe model](images/publish-subscribe.png)

## MQTT Connections
- MQTT uses TCP/IP to connect to the broker
- Most MQTT clients connect to the broker and remain connected even if they aren’t sending data. 
- MQTT clients publish a keepalive``*`` message at regular intervals (usually 60 seconds) which tells the broker that the client is still connected.

``*`` A keepalive (KA) is a message sent by one device to another to check that the link between the two is operating, or to prevent the link from being broken.

## Client Name / Client ID
 -All clients are required to have a client name or ID
 -The client's name is used by the MQTT broker to track subscriptions, etc.
- Client names must also be unique
- If you attempt to connect to an MQTT broker with the same name as an existing client, then the existing client connection is dropped
    - Since most MQTT clients will attempt to reconnect following a disconnect this can result in a loop of disconnect and connect 

# MQTT Topics
- MQTT topics are a form of addressing that allows MQTT clients to share information
- A Topic consists of a utf8 string that the broker uses to filter messages fo each connected client. It also consists of one or more topic levels.
- Each topic level is separated by a forward slash:
    ``myhome/groundfloor/livingroom/temperature``
- Think of it like folders in a file system. They're structured in a hierarchy that's similar to the directories in a unix system (because of the forward slashes!). 
- You can also think of it like logically grouping objects into less specific groups. You wouldn't really store your ``myhome`` in ``temperature``, now would you? It wouldn't make sense. This is why it's considered to be a *user-friendly* and self-describing naming structure of one's choosing.
- [(More on Topics here )](https://www.hivemq.com/blog/mqtt-essentials-part-5-mqtt-topics-best-practices/)

## Topic Requirements
- Case-sensitive
- Uses UTF-8 Strings
- Must consist of at least 1 character
- There is no default or standard topic structure, except for $SYS topic
    - $SYS topic is a reserve topic and is used by MQTT brokers to publish information about the broker
- No topics are created on the broker, except $SYS topic
- Doesn't start with $ (more on this later)

## Subscribing to Topics
- **A topic is created dynamically when either of the following occurs:**
    - Someone subscribes to the topic
    - Someone publishes a message to the topic with the retain message set to true
- **A client subscribe to a topic of a published message or it can use wildcard to subscribe to multiple topics simultaneously**
    - A wildcard can only be used to subscribe to topics, but it cannot be used to publish a message

## Consumers
- The consumer will greet this specific topic
Each topic must comtain 1 character and empty spaces are permitted. They're case-sensitive

## Wildcards
- In MQTT, you can subscribe to the exact topic of a published message. 
- To subscribe to multiple topics, you can use a wildcard.
- Note that wildcards can only be used to subscribe, not publish.
### Single Level: +  
A single-level wildcard replaces one topic level:  
`` myhome/groundfloor/+/temperature ``

### Multi Level: #
- Multi-level wildcard covers many topic levels
- The hash symbol represents the multi-level wild
card in the topic
- Multi-level wildcard must be placed as the last character in the topic and preceded by a forward slash

## Examples of wildcard matching
### For myhome/groundfloor/+/temperature:
- Matches:
    - myhome/groundfloor/**livingroom**/temperature
    - myhome/groundfloor/**kitchen**/temperature

- Does not match:
    - myhome/groundfloor/kitchen/**brightness**
    - myhome/**firstfloor**/kitchen/temperature
    - myhome/groundfloor/kitchen/**fridge**/temperature

### For myhome/groundfloor/#
- Matches:
    - myhome/groundfloor/**livingroom/temperature**
    - myhome/groundfloor/**kitchen/temperature**
    - myhome/groundfloor/**kitchen/brightness**
- Does not match:
    - myhome/**firstfloor**/livingroom/temperature

### MQTT Topics beginning with $
- You can name your MQTT topics as you desire, with one exception:
- Clients cannot publish messages to topics that start with $
- $-symbol topics are reserved for internal statistics of
the MQTT broker
- Examples of $-symbol topics:
    - $SYS/broker/clients/connected
    - $SYS/broker/clients/disconnected
    - $SYS/broker/clients/total
    - $SYS/broker/messages/sent
    - $SYS/broker/uptime

## Removing topics from the broker
- A topic is removed from a broker when the last client that subscribes to the topic disconnects and clean session is true
    - A **clean session** is a connection session in which the broker isn’t expected to remember anything about the client when it disconnects
    - For a **non-clean session**, the broker remembers client subscriptions and may hold undelivered messages for the client

## Last will message
- The idea of the last will message is to notify a subscriber that the publisher is unavailable due to network outage
- The last will message is set by the publishing client, and is set on a per topic basis which means that each topic can have its own last will message
    - The message is stored on the broker and sent to any client that subscribe to the topic, if the connection to the publisher fails
    - If the publisher disconnects normally the last Will Message is not sent

## Publish, Subscribe & Unsubscribe
- MQTT clients can publish messages as soon as they connect to a broker
- Each message must contain a topic that the broker can use to forward the message to the subscribing clients
- Messages typically have a payload that contains the data to transmit in byte format
    - The use case of the client determines how the payload is structured
    - Data can be binary data, text, XML or JSON data

## MQTT PUBLISH message fields
- Packet Identifier:
    - Uniquely identifies a message as it flows between the client and broker
    - The packet identifier is only relevant for QoS levels greater than zero

- Topic name:
    - A simple string that is hierarchically structured with forward slashes as delimiters
    `` myhome/livingroom/temperature ``

## QoS (Quality of Service)
- The QoS in MQTT is a number that indicates the Quality of Service of the message. 
- Three levels: 0, 1 and 2 (The higher the number, the greater the QoS)
- It basically indicates how much it guarantees that the message will reach the intended recipient. 
- Meaning that 2 QoS will be more guaranteed to reach its destination that 1

## MQTT PUBLISH
- When a client sends a message to an MQTT broker for publication, the broker reads the message, acknowledges the message (according to the QoS Level), and processes the message
    - Processing by the broker includes determining which clients have subscribed to the topic and sending the message to them
    - The client that initially publishes the message is only concerned about delivering the PUBLISH message to the broker
- It is the responsibility of the broker to deliver
the message to all subscribers
- The publishing client does not get any feedback about whether anyone is interested in the published message or how many clients received the message from the broker

![MQTT Publish](images/publish.png)

### Message fields
- Retail Flag: 
    - This flag defines whether the message is saved by the broker as the last known good value for a specified topic
    - When a new client subscribes to a topic, they receive the last message that is retained on that topic
- Payload
    - The actual content of the message
    - Can be virtually of any format, including encrypted data, binary data, XML, JSON, etc.

- DUP Flag:
    - The flag indicates that the message is a duplicate and was resent because the intended recipient (client or broker) did not acknowledge the original message
    - Only relevant for QoS greater than 0

## MQTT SUBSCRIBE
- To receive messages on topics of interest, the client sends a SUBSCRIBE message to the MQTT broker
    - The subscribe message contains a unique packet identifier and a list of subscriptions

![MQTT Subscribe](images/subscribe.png)

### Message fields
- Packet Identifier:
    - The packet identifier uniquely identifies a message as it flows between the client and broker
    - The client library and/or the broker is responsible for setting this internal MQTT identifier
- List of Subscriptions; 
    - A SUBSCRIBE message can contain multiple subscriptions for a client
    - Each subscription is made up of a topic and a QoS level
    - The topic in the subscribe message can contain wildcards that make it possible to subscribe to a topic pattern rather than a specific topic

## MQTT SUBACK
- To confirm each subscription, the broker sends a SUBACK acknowledgement message to the client
    - This message contains the packet identifier of the original Subscribe message and a list of return codes  
<br>
![MQTT Suback](images/suback.png)

### Message fields
- Packet Identifier:
    - The packet identifier uniquely identifies a message as it flows between the client and broker
    - It is the same as in the SUBSCRIBE message
- Return Code:
    - The broker sends one return code for each topic/QoS-pair that it receives in the SUBSCRIBE message

## MQTT Suback Return Codes
- E.g., if the SUBSCRIBE message has four subscriptions, the SUBACK message contains four return codes
- The return code acknowledges each topic and shows the QoS level that is granted by the broker
- If the broker refuses a subscription, the SUBACK message contains a failure return code for that specific topic  
<br>
![MQTT Suback Response codes](images/suback-response-codes.png)

- **NOTE:** After a client successfully sends the SUBSCRIBE message and receives the SUBACK message, it gets every published message that matches a topic in the subscriptions that the SUBSCRIBE message contained  
<br>
![MQTT messages](images/mqtt-pubsub.png)

## MQTT SUBSCRIBE
- This message deletes existing subscriptions of a client on the broker 
- The UNSUBSCRIBE message is similar to the SUBSCRIBE message and has a packet identifier and a list of topics  
<br>
![MQTT Subscribe](images/subscribe.png)

### Message fields
- Packet Identifier
    - The packet identifier uniquely identifies a message as it flows between the client and broker
    - The client library and/or the broker is responsible for setting this internal MQTT identifier 
- List of Topics
    - The list of topics can contain multiple topics from which the client wants to unsubscribe
    - You only need to send the topic. You can omit QoS.

### MQTT UNSUBACK
- **To confirm the unsubscribe,** the broker sends an UNSUBACK acknowledgement message to the client
- This message contains only the packet identifier of the original UNSUBSCRIBE message
- After receiving the UNSUBACK from the broker, the client can assume that the subscriptions in the UNSUBSCRIBE message are deleted  
<br>
![MQTT Unsuback](images/unsuback.png)

## Connection initiation (MQTT CONNECT)
- MQTT clients initiate connections to a MQTT broker by sending a CONNECT message
- If the CONNECT message is malformed``*`` (or too much time passes between opening a network socket and sending the message), then the broker **closes the connection**  
<br>
![MQTT Connect](images/mqtt-connect.png)

``*`` A malformed message is a CONNECT message that does not adhere to MQTT specs  

### Message fields
- Clientld
    - Identifies each MQTT client that connects to an MQTT broker
    - This Id should be unique per client and broker
- Clean Session
    - The clean session flag tells the broker whether the client wants to establish a persistent session or not
- Username/Password
    - MQTT can send a username and password for client authentication and authorization
- lastWillMessage
    - The last will message is part of the Last Will and Testament (LWT) feature of MQTT
    - If the client disconnects ungracefully, the broker sends the LWT message on behalf of the client
- KeepAlive
    - The keep alive is a time interval in seconds that the client specifies and communicates to the broker when the connection established
    - This interval defines the longest period that the broker and client can endure without sending a message
    - The client commits to sending regular PING Request messages to the broker; the broker responds with a PING response
    - This method allows both sides to determine if the other one is still available

## MQTT CONNACK
- It means "Connection Acknowledgement", the broker sends it after it receives a CONNECTION request
- It contains the following:
    - The session present flag:
        Tells the client whether the broker already has a persistent session available from previous interactions with the client
    - A connection return code:
        - This field contains a return code that tells the client whether the connection attempt was successful or not  
<br>
![Connack fields](images/connack.png)
![Connack return codes](images/connack-return-codes.png)

## MQTT Implementations
- Since MQTT is an ISO standard, there's going to be multiple implementations of it
- Most notable examples are HiveMQ and Mosquitto
- [There's actually a full list on Wikipedia if you want to know which ones exist](https://en.wikipedia.org/wiki/Comparison_of_MQTT_implementations)

## Java implementation
- **The HiveMQ MQTT client is what we'll be using, and is the most recommended way for the HiveMQ**
- [Implementation here: https://github.com/hivemq/hivemq-mqtt-client ](https://github.com/hivemq/hivemq-mqtt-client)
- This site provide documentation and example code for establishing connection with HiveMQ broker, publishing and subscribing to topics, etc.
- [Read the HiveMQ user guide for more information](https://www.hivemq.com/docs/hivemq/4.9/user-guide/introduction.html)
- [MQTT Study and Revision Workbook, by
Stephen Cope](www.steves-internet-guide.com)