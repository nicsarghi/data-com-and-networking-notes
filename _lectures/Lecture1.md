---
layout: post
title: Lecture 1
date: 2022/08/24
file: DataComm-Lecture1.pdf
dir: /lectures/
---  
# Part 1: Introduction to Networking Concepts

## Networks
Comprised of 2 or more computers that can communicate with each other 
Some books call them devices, but they're generally referred to as nodes in a network (any book on computer science will usually refer to them as one)

They can be a physical or virtual device. 
They don't have to be real physical devices, and can be a virtual network (although this works only with software)

--- 

## Hyperconnectivity
The world is extremely hyperconnected (IOT for example), most devices have internet access, like refrigrators, cellphones, cars, etc. The number of connected devices is expanding and we're at the highest number ever.

### Pros of Hyperconnectivity
- Less of a physical presence when you want to do anything (Think of Amazon.)
- Accessibility to knowledge, you can basically google anything! Or use wikipedia...
- Generally makes transactions convenient for businesses and for the user, since transactions can be remote and be done for either personal or business reasons. (Purchasing tickets to a concert, chatting with your friend, etc.)

### Cons of Hyperconnectivity
- Stressful interactions from social networks
- People are easily distracted from their smartphones. It's also easier for people to opt for their phones, so face-to-face conversations are more rare. **You know when old people keep hammering in to you that "It's difficult to put your phone down"? They're kind of right.**
- Everyone is online so youre EXPECTED to be online. People can also get stressed from not viewing your messages, etc.

--- 

## The evolution from Analog, to Digital, then to Internet... And finally IP

### Analog
- Think of telephone lines, telegraphs, answering machines and fax machines.
- Slower. So, so much slower. (Remember back then that computers used Dial-Up. Remember how slow that was? THAT'S analog.)
- Analog signals are still used in smart devices that need to transmit signals that use wavelengths. Sometimes analog signals are converted to digital signals, since we are in a "digital era".
- Analog usually refers to waves transmitted over a line (think of it like speaking through your phone - you're actually just transmitting the vibrations that you emit through sound!)
NOTE: Mathematically, this *also* implies that there are infinite values that can be processed within that wave (and instead of a binary digit, you need to measure this value, thus making it more difficult to process) 

### Digital
- Think of Computers, Smartphones, Internet and Broadband devices.
- Digital usually refers to data or information that is stored/transmitted in binary digits, and when it is being transmitted through a transistor that will process that input (and will usually perform instructions).
- _Some researchers use specialized machines that may do operations in other base numbers (>1). I think Ponce was referring to Ternary or trinary computers, which uses ternary logic instead of binary logic._

### Combining Analog AND Digital Segments
Analog segments can be converted to a digital signal, transmitited through the cloud and then converted back to analog input.  

![Illustration of how analog signals can be converted to digital signals](images/figure-1-2.png)


Digital devices dont necessarily have to be computers, they can be modems that are equally a part of network (which can use analog)

--- 

## Internet of Things (IoT)
These refer to physical objects that aren't necessarily computers. However they can "exist" in the digital world and thus are considered as a part of the internet.
These can include:
- Smart homes
- Cars
- Smart grid and Industrial IOT
- Smart farming, IoT tracking
- IoT medical devices

Hot-button topics for IoT devices:
- Data comm & security
"Current IoT devices may have inadequate security control and encryption capabilities, leaving devices vulnerable to potential threats. Threat actors can take advantage of device vulnerabilities, "
(https://cyber.gc.ca/en/guidance/internet-things-iot-security-itsap00012)
- IoT data analytics
Could be referring to harvesting the personal information of users for commercial purposes and how that affects privacy (https://www.priv.gc.ca/en/privacy-topics/technology/gd_iot_man/)

**Fun fact given by Ponce:**   
RFID tags can send data (usually through electromagnetic fields) to the internet, to a reader. This is how items can be purchased from a store, since the RFID usually transmits an ID number that gets sent to the reader.  

---

## The evolution of (long distance) communication
Snail Mail -> Email -> Real-Time Communication -> Social Media 

### Snail Mail
- How everyone transmitted information before the digital era. You take a letter and ship it through the mail, usually through a delivery service like Canada Post or FedEx. 
- Maybe even a carrier pigeon.

### Store and Forward Messages
- Email.

### Real-Time Communication
- Talking someone face to face (ie zoom or skype)
- Telephones
- Instant messaging
- Text messaging
- NOTE: Real-Time communication is subjected to increased bandwith (ie its more expensive for you to buy that from your isp). There's also more of a time delay and more packet loss. 

### Social Media (aka publish-subscribe)
- Social networking websites like Facebook, Twitter, Instagram. LinkedIn.

---
<div class="page-break"></div>

## The web's effect on business
- Since transactions can be done remotely, you better believe that brick-and-mortar stores can start doing business on the internet. 
- You can also create new types of commerce through the internet, where you can even sell things like web providing services or even delivery services done entirely online.
- **E-commerce gives us the ability to make business with remote customers over the internet.** It literally changed so much of how organizations do transactions.
- _Even marketing goods has changed because of how the web has affected our lives._

## IOT's impact on businesses
- Companies uses this technology to process information, which gets transmitted to the cloud and processed there.
- It's extremely useful for them to do so, since this information can be tracked pretty easily for the device and you can keep track of an ever growing collection of data (which is growing even faster now that you're using the IoT for analytics)
- Analytics can be used to identify waste much more easily.
- Identity (and thus authorizing) management is more complex
- Job market changes (ie jobs become irrelevant or change) because of the lack of need for real workers. (Think of how most people purchase things online nowadays, meaning that the demand for in person shopping is less than what it used to be.)

- NOTE: The IoT is a major player in the fourth industrial revolution. (Considering that it is used for the massive collection of data that is transmitted through the internet)

## The Cloud's impact on businesses
Provides of a lot of new cloud-based services. 

### IaaS: Infrastructure as a Service 
Provides and launches virtual machines, but can also be networking, storage, routers, switches, servers, load balancers, access points, etc. Usually they're cloud-based and pay-as-you go services.
- AWS EC2.
- Rackspace.
- Google Compute Engine (GCE).
- Digital Ocean.
- Microsoft Azure.
- Magento 1 Enterprise Edition*. 

Main clients are System administrators

### PaaS: Platform as a Service
Allows you to develop applications. Can be servers , databases, OS, storage, development tools, etc (all provided in an outside environment). 
- AWS Elastic Beanstalk.
- Heroku.
- Windows Azure (mainly used as PaaS).
- Force.com.
- Google App Engine.
- OpenShift.
- Apache Stratos.
- Adobe Magento Commerce Cloud. 

Main clients are developers

### SaaS: Software as a Service
Rents access to specific software applications. Acts as third-party software that you can acquire over the internet.
- BigCommerce.
- Google Workspace, Salesforce.
- Dropbox.
- MailChimp.
- ZenDesk.
- DocuSign.
- Slack.
- Hubspot. 

Main clients are end-users

### Anything as a Service AKA XaaS 
Basically specifies SaaS solutions as professional services. It's a general descriptor for a category of services that involve cloud computing or remote access 

- Security as a Service 
- Desktop as a Service (virtual desktop)
- Machine learning
- Function as a Service (serverless computing)
- Database as a Service 
- Blockchain as a Service 
- Backend as a Service
- Privacy as a Service

---
## Key Concepts
It's good to know what these are about primarily. If you need to know something by heart, it's definitely these words. Study what they mean by looking at the original slides and make sure you understand them enough to the point where you're able to explain them.

- The hyperconnected world / hyperconnectivity
- Internet and IP connectivity
- Evolution of communication: Snail mail, store-and-forward messaging, social media, and real-time
messaging
- How the web, the Internet of Things (IoT), and cloud computing have
transformed business

---

# Part 2: Network devices and components

## Node connection in a network
### NIC
Refers to a Network Interface Card. There's two types, aka wireless and wired. 
- Wireless ones can contain an antenna (like smartphones or wireless dongles) which can transmit or recieve radio-frequency waves. You can connect to WI-FI with a wireless NIC
- Wired ones refer to cards that you plug the ethernet cable into. Relies on a wired input jack and wired LAN technology

### Linking Medium
Refers to how usually how the node connects to the network (where the communication is established and where data travels between nodes. This can be cables or some wireless medium like air. 

This may illustratie better of what linking mediums are:
![Wireless and cable mediums as well as a detailed visualization of them](images/lecture1-mediums.png)

## Notes on Bps and bps
Remember that the capitalization of the b is important!
- bps -> BITS per second
- Bps -> BYTES per second

## Medium Cable types (Slowest to Fastest)
### Telephone Lines
- Nodes can be connected in a network through telephone lines, they are still used but are uncommon.
- There are 2 visible wires!
- DSL
- SPEED: 1 Gpbs  
![Telephone line cables](images/lecture1-telephone-wires.png)

### Ethernet Cable
- 8 wires, twisted
- SPEED: 1000 Mbps - 10 Gbps  
![Ethernet cable](images/lecture1-ethernet-cable.png)

### Coaxial Cable
- Videotron uses these 
- SPEED: 2 Gbps  
![Coaxial cable](images/lecture1-coaxial-cable.png)

### Fiber optic cable
- SPEED: 2-40 Gbps 
(as technology improves we generally get more bits per second, and thus its faster)  
![Fiber optic cable](images/lecture-1-fibre-optic.png)


## Wireless connections
### WIFI
The most common access point at home.
- 20 m indoors, 150 m outdoors
- SPEED: 5.5 - Mbit/s

### Bluetooth 
Sent to a computer through a device.
- RANGE: Common range is 10 m (0.5 - 100 m)
- SPEED: 2.1 Mbit/s

### Near Field Communication (NFC)
Used in cards often (credit cards, opus cards.) It's really useful for payments.
- RANGE: 20 cm
- SPEED: 424 kbit/s
- NOTE: It's slow but you dont need to connect a cable to a credit card, so you're trading off convenience for speed.
- It usually stores a small chip with an ID to transmit basic information to a reader.

** NOTE that this technology needs to suit the business needs. **

--- 

## Network components and devices

### Hub
A hub usually recieves a packet from one node and transmits it to the other nodes connected to the hub  

![Hub transmission animation](images/bus.gif)

### Switch 
- There's an issue when two devices send data through a hub and where there may be a collision between the two devices. This is especially true if you have two links connected to that hub, and one connection is "using up" the current, to the point where a second connection isn't able to send information.
- A way to solve this is to determine the time in which each device can communicate (ie switching between times). You can also use collision detection.
- This is how the switch usually handles communication with multiple nodes at the same time. It is not a common bus like a hub, but instead it maps connections via individual channels (it's a little more complicated than that, and actually use a much more sophisticated routing method for forwarding packets to a destination port. https://azizozbek.ch/blog/2018/02/how-switch-works/)

<div class="page-break"></div>

### **A switch routes packets to another destination through a channel**
![Switch symbol](images/switch-diagram.png)

<div class="page-break"></div>

### **Example switch transmission:**
![Switch transmission animation](images/switch.gif)

Illustration of a switch symbol, and how one may look
![Switch symbol and an actual switch](images/lecture1-switch.png)

### Bridge 
- A bridge will have two different LANS connected to eachother in a network. This is NOT a router, which is used for internet/networking.
- EX: If you create a virtual machine, there were be an option to create a virtual network in VMware with a virtual bridge to the LAN at home.

## Router
The router is a piece of network hardware that connects a local network to the internet. To be more precise, it exists to connect two packet-switched networks. 
Illustration of a router symbol, and a picture cisco router 
![Router symbol and cisco router](images/lecture-1-router.png)

## Cable Modem
Type of network bridge that usually connects a router to an ISP. Allows for high-speed internet by using coaxial cables that connect to the modem.