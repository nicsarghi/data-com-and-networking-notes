---
layout: post
title: Lecture 4
date: 2022/09/7
file: DataComm-Lecture4.pdf
dir: /lectures/
---  

# Concepts on networking and protocols
- The goal for now is to explore each layer of the OSI model (and by extension the TCP/IP stack)
- We'll be covering the Data Link and Physical Layer (aka the network access layer in tcp/ip suite)
- Internet Protocol suite will be mainly focused on

## Data encapsulation
- Protocols define a set of rules for communicating between nodes
- On each layer of the OSI model, there's always going to be metadata, AKA the control information so that you can properly send it.
- The control information is called the header, since it's always in front of the data to be transmitted
- Groups the data in each layer into distinct segments. The point is to isolate (encapsulate) and layer it on top of eachother. **This consists of adding delivery information on each layer**

- For example, the data in the TCP/IP stack can contain data about the state of the connection.
![Encapsulation](https://www.researchgate.net/profile/Isara-Anantavrasilp/publication/49288737/figure/fig4/AS:669528941924353@1536639547060/Packet-encapsulation-TCP-IP-architecture-encapsulates-the-data-from-the-upper-layer-by.png)


## Protocol Data Unit
- Application layer -> Data
- Transport layer -> Datagram (UDP) / Segment (TCP)
- IP -> Packet
- Net Acc.layer -> Frame
- Physical -> Bits

## TCP Header
![TCP header](images/tcp_header.png)

## UDP Header
![UDP header](images/udp_header.png)    

## IP Header
![IP header](images/ip_header.png)

## Ethernet frame
![Ethernet Frame](images/ethernet_frame.png)
 
# IP address classes
IPv4 addresses are four segments of data that range from 0-255. Basically 4 8 bit values (255.255.255.255). It's a 32 bit value. 
In networks, we use the subnet mask to determine which part of IP is the network and which part of the ip is the host.

There are 5 classes that defines the ranges that each segment can take on, and it depends on each bit. Class A, B and C are the widely used ones. All classes have a corresponding subnet mask:
- **Class A:** 255.0.0.0 -> Range: ``[ 0-127 ].[ 0-255 ].[ 0-255 ].[ 0-255 ]``
- **Class B:** 255.255.0.0 -> Range: ``[ 128-191 ].[ 0-255 ].[ 0-255 ].[ 0-255 ]``
- **Class C:**  255.255.255.0 -> Range: ``[ 192-223 ].[ 0-255 ].[ 0-255 ].[ 0-255 ]``
- Class D: Used for multicasting (sending traffic to a group of devices such as in a distributed video or a web conference session)
- Class E: Used for experimentation 

## Private and Public IPs
- Public IP: IP address given to you so that you're able to access the internet. Usually assigned to a router and shared between hosts on a network.
- Private IP: Not routed on the Internet, so no traffic can be sent to them either from the Internet. In a way, you can avoid collisions on a network since each private network won't collide with the IPs of the other networks (ie no confusion between two nodes with the same IP as long as theyre in different networks.)

Here are IP ranges reserved to private networks; these addresses are intended for use in closed local area networks, and no one globally controls the allocation of such addresses.
- A: 10.0.0.0 to 10.255.255.255 (1 network)
- B: 172.16.0.0 to 172.31.255.255 (16 networks)
- C: 192.168.0.0 to 192.168.255.0 (256 networks)

# Network Access Layer
Bottom layer aka Layer 1 of the TCP/IP model. Ensures the following:
- Reliability: Availability and Integrity of data transmission
- Availability: Uptime AKA the physical link access to communication line
- Integrity: Whether the binary data made it to the destination intact. It has to be accurate.

## What it consists of:
- Consists of Layer 1 and Layer 2 of the OSI model. 
- It basically covers how a digital signal (aka a sequence of binary values or a **bit sequence**) will be sent. 
- It also covers the physical mediums needed to transmit that data, such as cabling or air. 
- This layer also defines a frame format that the binary sequence need to conform to. (aka how binary data is *encoded* to make a frame.)

## Ethernet Frames
- [Ethernet operates in the data link layer and the physical layer. It is a family of networking technologies that are defined in the IEEE 802.2 and 802.3 standards.](https://www.ciscopress.com/articles/article.asp?p=3089352&seqNum=4)
- **An Ethernet frame defines the encapsulated data defined by the Network Access Layer.** An Ethernet frame starts with a header, which contains the source and destination MAC addresses, among other data. The middle part of the frame is the actual data, and the end of the frame is the Frame Check Sequence (FCS) ( [source](https://study-ccna.com/ethernet-frame/) )

## MAC Address
2 portions that total into 48 bits:
- Manufacturer: first 24 bits (8F:CA:B4)
- Unique serial number assigned by the manufacturer: 24 bits: last 24 bits (E3:67:EA)
Embedded on your network interface card. Literally burned into your network interface card
- A TCP/IP packet must contain the destination’s MAC address.
- As each packet arrives at the NIC, the header is examined to see the target
MAC address. If it's the MAC address of the NIC, the packet is passed up the protocol stack. If it's a different address than the NIC, then it gets discarded.

## Frames and Mac Addresses
- Inside of a switch, there's a component that keeps track of a table that contains all of the mac addresses and ports.
- Ex: Mac 1 is connected to port 3, the table will display mac 1 | port 3
- The header of the frame defines the source and destination MAC addesses that will be used to transmit the frame data to the corresponding destination. **This is the bit sequence that the switch needs in order to route data accordingly**
- Packets are received by a network card.

# IEEE 802.3
- The standard for the Physical layer and data link layer’s media access control (MAC) of wired Ethernet. Word salad aside, this is the standard that defines how ethernet connections can communicate between devices.

## CSMA/CD
- CSMA/CD refers to Carrier-Sense Multiple Access with Collision Detection. It was defined in the earlier versions of 802.3. 
- It basically solved problems concerning collision detection, as in cases where two devices may try to use a data channel at the same time. 
- What it does is determine how long a device should wait if a collision occurs.   
- No longer needed in full-duplex connections
- Still supported for backward compatibility, half-duplex
connections

![Example of CSMA managing the transmission of data from two devices](images/ethernet-carrier-sense-multiple-access.png)
![Example of collision detection](images/collision-detection.png)

## Full-duplex Connections
- **Sending AND receiving data over one channel**
- Full-duplex is used to describe communication where two nodes talking to each other are able to send and receive data at the same time.
- Ideal scenario, as there's NO threat of a collision occuring. 
- A full-duplex device is capable of bi-directional network data transmissions at the same time.  
- Note that the device must be full-duplex, meaning that the network interface card must support it.

![Full-duplex and half-duplex](https://www.comms-express.com/infozone/wp-content/uploads/2017/01/Half-vs-Full-duplex-e1657701720121.jpg)

## Half-duplex Connections
- **Sending OR receiving data over one channel**
- Data can move in both directions, **just never at the same time**. Both devices are capable of transmitting and receiving so when one device is sending, the other is receiving.

## Collision domain
- Describes a part of a network where packet collisions may occur. When two devices on a shared network send packets at the same time, you get a collision.
- When they do collide, they have to be discarded.
- Collisions occur often in a hub environment because all devices connected to the hub are in the same collision domain. Only one device may transmit at time, and all the other devices connected to the hub must listen to the network in order to avoid collisions. Total network bandwidth is shared among all devices.
- Heres an example of collision domains, which are illustrated by the boxes with a red outline:
![Map of collision domains](https://geek-university.com/wp-content/images/ccna/collision_domain_example.jpg)

## More on collisions
- Back then, when we still used hubs (which uses the bus topology), collision was a pretty big issue. Even if they're not optimal nowadays.
- The entire collision domain WAS the giant connecting cable on the hub.
- One technique was to use a bridge between two hubs, which created two collision domains, and thus increased the network capacity.
- Eventually to avoid collisions altogether, we ended up moving to switches.
- There's also the inclusion of protocols that prevent collisions, such as CSMA/CD. 
- For example, some hubs can usually send data at random times, or checks if the channel is busy.

## Switches and collision domains
- A switch links all ports together. Each connection to the switch is its own collision domain.
- There can still be collisions in a switch, if two computers send data at the time (this is the case in half-duplex systems, where one is sending and receiving at the same time)

# Misc information briefly touched on in class

## IP Address
- Logical address that is determined by software.
- The network configures it for you through DHCP

## Ports
- Number that can range from 0-something
- TCP header will contain data about the source and destination port
- UDP header as well
- UDP doesnt care about lost packets or the order in which packets are sent, TCP ensures theres an order 
- TCP usually contains awknowledgement and connection state in the header
- IP header contains the source address, destination address, etc.
- Take note of the Ipv4 address classes and the types of private address ranges that exist.

## LAN Hubs vs LAN switches
- LAN hubs will usually broadcast data to other devices while switches will sned it to the according path.
![LAN hubs vs switch](images/lan-hub-vs-switch.png)




