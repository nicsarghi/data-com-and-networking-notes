---
layout: post
title: Lecture 14
date: 2022/11/02
file: DataComm-Lecture14.pdf
dir: /lectures/
--- 
# MQTT and Pi development
## Developing on a remote pc - Raspberry Pi
- Developing directly on the pi is SLOW. It sucks. so much. Ponce posted a guide on how to get remote development working, [you can view it here](https://pi4j.com/getting-started/developing-on-a-remote-pc/) 
- I've personally had issues running javafx apps through an x-terminal. What I did was ethernet connection to your pi (basically set up two static ips for your computer and your pi), and then connect to your pi through VNC. [You can use this tutorial for Windows](https://www.circuitbasics.com/how-to-connect-to-a-raspberry-pi-directly-with-an-ethernet-cable/) 

## FutureTask (threads in java)
- Kind of like promises in JS, it's a way to have asynchronous java code.
```
A cancellable asynchronous computation. This class provides a base implementation of Future, with methods to start and cancel a computation, query to see if the computation is complete, and retrieve the result of the computation. The result can only be retrieved when the computation has completed; the get methods will block if the computation has not yet completed. Once the computation has completed, the computation cannot be restarted or cancelled (unless the computation is invoked using runAndReset()).

A FutureTask can be used to wrap a Callable or Runnable object. Because FutureTask implements Runnable, a FutureTask can be submitted to an Executor for execution. 
```
- [Documentation for FutureTask here](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/FutureTask.html)

## Debugging threads
- Ponce asked the class about debugging multithreading for the lab exercise. It's nearly impossible with the current tools we have.
- Debugging threads, especially when there's a large quantity of them, can be extremely difficult. System.out.println() is your best friend in this case, even if it's not the most efficient method.
- Ponce mentioned IntelliJ is still pretty good for debugging multithreading too. [This answer on StackOverflow might be of use](https://stackoverflow.com/questions/27784413/how-to-debug-a-multi-threaded-app-in-intellij)

## MQTT Project
- We'll be using a cloud service for it, so that it's generally easier for you to transmit data.
- HiveMQ is providing the servers that we'll use for the project.
- Select the Java programming language in the *Getting started with MQTT* section. It will give you a guide for the Maven configurations you need to get it to work.
- Use the free version on HiveMQ (aka create a free cluster). Your Java client will connect to HiveMQ w/ your username and password.
- You'll need to subscribe to the broker, and publish messages to it.

### Components
- Motion sensor
    - There's a little white box-like component in the freenove kit. 
    - Connect a female-to-male cable to the motion sensors so that you can then connect it to the breadboard.
- Temperature and Humidity
- Motion
- LED
- Camera
    - The crash can be fixed by adding a line to the code
    - Gui mentioned ``--preview``?

### MQTT Updated Slides
- The MQTT slides are updated and published to Moodle. 
- Lecture 12 has been completed as a result.

### QOS and Encrypting topics
- The topic is usually in plaintext, but you can encrypt the topic in the network. Is not necessary, but you can do it on the side.
- [Here's an article that allows you to use TLS encrypted communications for your MQTT broker.](https://www.hivemq.com/blog/end-to-end-encryption-in-the-cloud/)

### Brief clarification on QOS
- QOS (quality of service) describes the priority of a message transmitted through network (and cloud, IOT services, etc.). Not as important for the project, but good to know.

### Symbols
- Make sure to use the normalized unicode symbols and not the windows onces when sending date to HiveMQ. (Remember that messages in HiveMQ have to follow the utf-8 standard!)
- Remember to Validate the user/external input. Don't trust what the client tells you to be true.





