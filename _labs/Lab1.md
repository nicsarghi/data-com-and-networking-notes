---
layout: post
title: Lab 1
date: 2022/08/24
file: DataComm-Lab1.docx
dir: /labs/
---

# **Objectives**
- Install OS on Raspberry Pi
- Configure the Raspberry Pi
- Update the Raspberry Pi  

# **Instructions**
1. Remove the microSD card from the Raspberry Pi or the plastic sac and insert it into the USB microSD card reader. Note: if the Raspberry Pi is connected to the case, you may need to remove the Pi from the case. Note also that when the text on the card reader is facing upward, the pins on the microSD card should also be facing upward.
2. Insert the USB microSD card reader containing the microSD card, into a USB port on the workstation, then follow the instructions at <https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up> to use Raspberry Pi Imager to install the latest version of Raspberry Pi OS on the microSD card.
3. Remove the microSD card from the USB microSD card reader and insert it into the Raspberry Pi (RPi); next, connect the RPi to the RPi case, then follow the instruction at <https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up/3> to connect the RPi to the keyboard, mouse, and monitor. Note: the RPi is a Raspberry Pi 4.
4. Follow the instructions at <https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up/4> to start and set up the RPi. 
5. Open the RPi terminal by clicking on the terminal icon in the top left corner of the RPi desktop, and execute the following command: 
   **sudo raspi-config**
6. Choose “3 Interfacing Options Configure connections to peripherals”.
7. Choose “I2 SSH         Enable/Disable remote command line access using SSH”.
8. The next window says, “Would you like the SSH server to be enabled?” Select Yes, then press Enter.
9. Repeat step 6 and select “I3 VNC         Enable/Disable graphical remote access using RealVNC”.
10. The next window says, “Would you like the VNC Server to be enabled?” Select Yes, then press Enter.
11. On the main menu, select “Display Options	Configure display settings” and press Enter. On the screen that appears, select “D5 VNC Resolution	Set resolution for headless use” and press Enter. From the list that appears, select the best resolution that your monitor supports (possible choices are 1280x1024 or 1600x1200 or 1920x1080) and press Enter.		
12. Select <Finish> to quit the RPi config.
13. The following should appear on the next screen: “Would you like to reboot now?”. Select “No” and press Enter. 
14. Use the command line terminal to edit /boot/config.txt by typing the following command:
   **sudo vim.tiny /boot/config.txt**

   Uncomment the line:
   **hdmi\_force\_hotplug=1**

   Add the lines just below the commented lines that start with “hdmi\_group” and “hdmi\_mode”, respectively. 
   **hdmi\_group=2
   hdmi\_mode=73**
   Note: “hdmi\_mode=73” assigned the desired resolution. Use the table shown on the site at <https://www.raspberrypi.com/documentation/computers/config_txt.html> to determine the value for the ideal resolution and frequency that your monitor supports. Use the table shown just below the line: “These values are valid if hdmi\_group=2 (DMT)” to find the appropriate value for the variable “hdmi\_mode”, then if needs be, change the 73 to that value.**

   Comment out these lines:
   **#dtoverlay=vc4-kms-v3d
   #max\_framebuffers=2**
15. Reboot the RPi by executing the following command:
   ***sudo reboot***

16. When the reboot completes, login to the RPi and connect it to Eduroam Wi-Fi. The instruction for connecting ChromeBook to Eduroam WiFi network, given at <https://www.dawsoncollege.qc.ca/information-systems-and-technology/articles/wireless-access>, may be useful to help you connect the RPi to the WiFi. 
17. After connecting the RPi to the Wi-Fi, determine the IP address of the RPi by hovering the mouse over the Wi-Fi icon in the top right corner of the RPi desktop, or by opening a command line terminal and typing the following command:
   ` `***hostname -I***  

   **Make note of the IP address because you are going to need it later**.
18. Disconnect the monitor, the keyboard, and the mouse from the RPi and reconnect them to your workstation.
19. Check if RealVNC viewer is installed on your workstation. If it’s not installed, download it from this link: <https://www.realvnc.com/en/connect/download/viewer> and install it.
20. Use RealVNC Viewer to connect to the RPi, to establish a remote desktop. Alternatively, you can ssh into the RPi by executing the Windows ssh command at the command line (PuTTY or any ssh client can also be used). 
21. Update your RPi by opening a terminal and executing the following commands:
```
sudo apt update
sudo apt full-upgrade
```
