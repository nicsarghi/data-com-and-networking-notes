﻿---
layout: post
title: Lab 3
date: 2022/08/31
file: DataComm-Lab3.docx
dir: /labs/
---	

# **Objectives**
1. Identify your device’s public IP address
1. Use network monitoring tools such as netstat, ss, nmap and tcpdump to ascertain information about open ports, sockets, and network services
1. Use tcpdump to examine TCP and UDP packets
1. Configure static Class A, B, and C addresses
# **Ports and sockets**
Recall that a network port is a software-defined number that receives or transmits communication data for a specific service, whereas a network socket is one endpoint of a two-way communication link between two programs running on a network. A socket address is a triad of transport protocol (TCP or UDP) IP address and port number. In this lab, we will use commonly used network tools to ascertain information about opened ports, services running on the network devices, the state of sockets, etc.
# **Instructions**
Connect the RPi to the WiFi and use RealVNC viewer to establish a remote desktop on the RPi. If you are unable to establish a remote desktop on the RPi, the most likely reason is that the IP address leased to your device the last time it connected to the WiFi network, has expired. If that’s the case, you will need to connect the keyboard and mouse to the RPi and execute the following command at the command line terminal: **sudo ifup wlan0**

Verify that you are connected to Eduroam by executing the following command: **iwconfig**

Next, execute **ifconfig** command and note the IPv4 address that is assigned to the wlan0 interface. Record the IP address because you will need it later. Next, disconnect the keyboard and mouse from the RPi and **re-connect the keyboard and mouse to the lab computer.**

Type in your answers to the questions in this document. **Highlight your answers using yellow text highlight colour**. You will likely be tested on the content of this lab; thus, it is important that you understand the concepts involved.

Most of the network tools we will be using in today’s lab are included in the *net-tools* package. Execute the following command to install the net-tools package:
***sudo apt install net-tools***

**Netstat** command is used for examining network connections and various network settings and statistics. Open the RPi command line terminal and use it to execute the commands shown below.

1. Run the command “**man netstat**” and observe the command line options available for netstat.
 
1. Execute the command “**netstat -natu**” and observe the output.
   1. What is the role of the command line options n, a, t and u?
   1. For the established sockets, what are the services running at the respective ports?
   1. What are the open ports on the RPi that are listening for connection requests?
   1. Why is it that no connection state is shown for UDP sockets?
 
1. Execute the command “**netstat -s**” and observe the output.
   1. What are the Transport Layer protocols shown in the output?
   1. For IP protocol, were any of the incoming packets discarded?
   1. What is the most common reason for packets not being delivered? 

**SS** command is another utility to investigate sockets.

1. Run the command “**man ss**” and observe the command line options available for ss.
1. Execute the command “**ss -atu**” and observe the output.
   1. What’s the role of the a, t and u command line options of ss?
   1. What is the main difference between the socket information in ss output compared to that of netstat?
   1. What is mdns service and what is its role and why is it required?

**Tcpdump** is a packet filtering tool that can be used to ascertain useful information about network packets. It listens to network traffic and prints information based on criteria you define.

1. Execute the command “**man tcpdump**” and observe the available command line options.
1. Execute the command “**sudo tcpdump host x.x.x.x -n -c 5**” 
   (Note: x.x.x.x should be replaced with the actual IP address of your RPi), to examine packets sent and received to/from your RPi.
   1. What is the role of the “host”, -n and -c options?
   1. Use information presented at <https://networklessons.com/cisco/ccie-routing-switching-written/tcp-header> to aid you to interpret the information in the tcpdump output.
   1. What network services are involved in the packets sent and received by your RPi?
1. Execute the command “**sudo tcpdump -nX -c2 port 443**” and open a browser window on the RPi and observe the output.
   1. What additional information appeared in this command compared to the previous tcpdump command? What does this additional information represent?
1. Execute the command “**sudo tcpdump -nX -c2 port 53**” then visit bbc.com website using a browser running on the RPi.
   1. The packets captured with this command are related to what service? How do you determine this information?
   1. There is a notable difference concerning the “additional information” appearing in the tcpdump output for port 53 as opposed to that for port 443. What is this notable difference?
# **Determine your device's Public IP address**
Recall that private IP addresses are non-routable, i.e., a private address is required for your device to connect to the Internet. To determine the public IP address that your RPi uses, open a browser on the RPi, navigate to google.ca, type the following in the search bar: “What is my IP” and press enter.

1. What is the public IP address assigned to your RPi?
1. Inquire from your classmates whether the public IP address assigned to their RPi is the same or different from that of your RPi.
   1. What is the result of your inquiry?
   1. What protocol is used to assign the public IP address to the devices?
# **Setup and configure a network**
This portion of the lab involves setting up a network and configuring the hosts in the network to have static IP addresses. You will work in groups of 4, 5 (depending on the number of switches available) for this portion of the lab. Follow the instructions given below:

Recall that there are 3 blocks of private IP addresses: 
10.0.0.0 to 10.255.255.255 
172.16.0.0 to 172.31.255.255
192.168.0.0 to 192.168.255.255

1. Form a group of 5 members consisting of classmates working adjacent to your workspace. 
1. Decide with your team members on the private IP block you will use for the team’s network, then each team member must choose a **unique** class C IP address, within the selected IP address block. 
1. Connect your RPi to the switch assigned to your group, using the CAT6 twisted-pair cable you are provided.
1. Connect the keyboard and monitor to the RPi and follow the instructions given below to configure the RPi with the private class C IP address you choose:
   1. Edit */etc/dhcpcd.conf* file using sudo and a text editor (ex. vim.tiny or nano). Go to the end of the files and add the following lines:
   ```
      #Set up static IP address
      interface eth0
      static ip\_address=X.Y.Z.W/24
      static routers=X.Y.Z.1

      interface wlan0
      static ip\_address=X.Y.Z.W/24
      static routers=X.Y.Z.1
   ```
      **Note**: X, Y, Z and W should be replaced with the actual values of the IP address you chose.
   1. Save and exit the file, then reboot the RPi by executing the following command:  
      ``sudo reboot``
   1. When the RPis reboot, verify that you can ping all your group members RPis
   1. What is the netmask and the broadcast address of the RPi (you can use the ``ifconfig`` command to determine this)? 

**Nmap** (Network Mapper) is a popular utility used for network discovery, security auditing and administration. The default Nmap scan shows the ports, their state (open/closed), and protocols. It sends a packet to 1000 most used ports and checks for the response.

1. Run the “man nmap” and observe the available command line options.
1. Execute the command “**nmap -sn X.Y.Z.0/24**” (**as indicated above**: X, Y and Z should be replaced with the actual values of the IP address you chose) to identify the hosts currently attached to the network. 
   1. What is the meaning of **X.Y.Z.0/24**?
   1. What is the role of -sn command line option for nmap?
   1. How can “**nmap -sn X.Y.Z.0/24**” command be used in combination with **grep** and **wc** to print the number of hosts connected to the network?
   1. What is the number of hosts connected to the network?
   1. Execute the command “**nmap X.Y.Z.0/24**” to see the open ports on the hosts connected to the network. 
   1. Are there any open UDP port on any of the hosts attached to the network? How can you determine this information using **grep**?

### **Configure the RPi with Class B and C static IP address**

Each member of your team must choose unique class B private address that’s within the private IP address block your team selected. 

1. Configure your RPi with a static private class B IP address. To do so, in */etc/dhcpcd.conf*, change “static ip\_address=X.Y.Z.W/24” to “static ip\_address=X.Y.Z.W/16”.
   1. Use ping and nmap to verify that you can reach all your team members RPis.
   1. What is the netmask and the broadcast address of the RPi when it is configured with a class B address?
   1. How does the netmask and the broadcast address compare when the RPi had a class C IP address?
1. Configure your RPi with a static private class A address.
   1. What changes did you make in /etc/dhcpcd.conf file to configure a class A IP address?
   1. What is the netmask and the broadcast address of the RPi when it is configured with a class A address? 
1. Delete all the lines you added to the /etc/dhcpcd.conf file (starting with the line: 
   “#Set up static IP address” to the end of the file). This is necessary to enable the RPi to be able to connect to the WiFi network.
1. Shutdown the RPi by executing the command: “**sudo shutdown -h now**”.
# **Grade and Submission**

1. Disconnect the RPi from the switch and re-package to switch and Ethernet cables.
1. **Reconnect the keyboard and monitor to the workstation and verify that the mouse and keyboard function correctly.**
1. **Return the converter (monitor), switch, and twisted pair cables to your teacher**

1. When you complete the lab, please submit the Word document containing your answers to the questions to **Moodle** via the **Assignment Week 3 – Networking part 2** link.**


We will discuss the answers to the questions during the latter part of the lab.
