---
layout: post
title: Lab 2
date: 2022/08/31
file: DataComm-Lab2.docx
dir: /labs/
---	

# Some important notes
Some people have reported issues connecting to eduroam, so it might be a good idea to refer to this [Github for the proper configuration settings](https://gist.github.com/S4more/deb1eb04518d34743f01b964b5ffd3ff) 

# Objectives
1. Use network admin tools to test the connectivity of Raspberry Pi (RPi) on a network, and to determine network parameters such as IP address, netmask, etc.
2. Know what layers of the TCP/IP stack, the OSI reference model, and admin tools such as ping, tracert, nslookup, and arp operate. 

# Instructions
**Configure the RPi to connect to Eduroam WiFi**
Follow the instructions given below to configure the RPi to connect to Eduroam WiFI:  

1. Edit `/etc/network/interfaces` and add the following lines to the end of the file:

````
auto lo wlan0
iface eth0 inet dhcp
auto wlan0
allow-hotplug wlan0
iface wlan0 inet dhcp
        pre-up wpa_supplicant -B -D wext -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf
            post-down killall -q wpa_supplicant

````

2. Add the following lines at the end of */etc/dhcpcd.conf* file:

```
interface wlan0
env ifwireless=1
env wpa_supplicant_driver=wext, nl80211
```

3. This section splits into the original instructions or Gui's instructions. Gui's instructions does not involve hashing your passwords, so beware.

### Gui's instructions
- After logging into the RPi, edit ``/etc/wpa_supplicant/wpa_supplicant.conf``
 and paste the following config:

````
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=netdev
country=CA

network={
  ssid="eduroam"
  key_mgmt=WPA-EAP
  eap=PEAP
  phase2="auth=MSCHAPV2"
  identity="yourUsername@dawsoncollege.qc.ca"
  password="yourPassword"
}
````
- Now, edit "yourUsername" and "yourPasword" for your Dawson e-mail and password
- Save and reboot your pi.

### Original instructions

- Login to the RPi after it reboots and add the following lines to the end of */etc/wpa\_supplicant/wpa\_supplicant.conf:* 
```
network={
    ssid="eduroam"
    priority=1
    proto=RSN
    key\_mgmt=WPA-EAP
    pairwise=CCMP
    auth\_alg=OPEN
    eap=PEAP
    identity="YOUR\_DAWSON\_USER\_NAME"
    password=hash:YOUR\_PASSWORD\_HASH
    phase1="peaplabel=0"
    phase2="auth=MSCHAPV2"
}
```

- **Note**: Replace YOUR\_DAWSON\_USER\_NAME with your actual Dawson username that you use for Eduroam; the double quote (“”) should be kept where indicated in the file. 

- Generate the hash of your Dawson password using the following command:  
``echo -n 'YOUR\_PASSWORD' | iconv -t utf16le | openssl md4``

- Replace YOUR\_PASSWORD with your actual Dawson password and keep the single quote (‘ ‘).

- In ``/etc/wpa\_supplicant/wpa\_supplicant.conf`` replace ``YOUR\_PASSWORD\_HASH`` with the actual hash you generated; thus, the line containing the password should be something like the following, but with a different hash:  
``password=hash:e7b8ff9feb451a2dad4e6de84f394adb``

4. Delete the command containing your password from history by executing the following commands: 
````
$ history |tail
$ history -d HISTORY\_NUMBER*
````
- **Note**: HISTORY\_NUMBER must be replaced by the actual number of the history item you want to delete.

### **This is where the split between the two instructions ends.**

- **Note:** If you're somehow having trouble turning the interface off (make sure everything configured properly!), then try running ``ip link set wlan0 down``

5. Bring down the RPi wireless interface, then bring it back up by executing the commands:
```
sudo ifdown wlan0
sudo ifup wlan0
```

6. Verify that you are connected to Eduroam by executing the following command: ``
````
iwconfig
````

- Next, execute the ``ifconfig`` command and note the IPv4 address that is assigned to the wlan0 interface. Record the IP address because you will need it later.

7. To prevent periodic disconnection from the WiFi, do the following commands, which add a task to ping Google DNS server (8.8.8.8) periodically:

```
crontab -e
``` 
- After selecting the editor you want to use for crontab (1 for nano or 2 for vim.tiny), add the following line to the end of the crontab file:
``\*/1 \* \* \* \* ping -c 1 8.8.8.8``

- For an explanation of crontab syntax, refer to the crontab tutorial available at: <https://ostechnix.com/a-beginners-guide-to-cron-jobs> 

- **QUESTION:** *What does the crontab entry (i.e., \*/1 \* \* \* \* ping -c 1 8.8.8.8) do?*

- Connect to the RPi using RealVNC

- Disconnect the keyboard and mouse from the RPi and **re-connect the keyboard and mouse to the lab computer.** Use RealVNC viewer to connect to your RPi. 

- Open the RPi command line terminal and use it to execute the commands shown below. 

- Type in your answers to the questions in this document. **Highlight your answers using yellow text highlight colour**. You will likely be tested on the content of this lab; thus, it is important that you understand the concepts involved.

- Run the ``ifconfig -a” command``, record the requested information, and explain the roles of the requested items in a few words.
 
   1. The name of the interface the RPi current uses for network connectivity:
   1. MAC address:
   1. IPv4 address:
   1. Subnet mask:

- Run the command: ``ip route show`` to determine the default gate of the LAN.
   1. What is the default gateway of the LAN?
   1. What is the role of the default gateway?
-- The ``ping`` command can test the connection between two network devices on an IP network. Ping will return the round-trip time from your device to the device being ping and back again. The first line of ping output shows the Fully Qualified Domain Name (FQDN) of the device being ping, followed by the IP address.  Execute the command ``ping -c 5 google.ca``
   1. What’s the IP address associated with google.ca?
     
   2. How is the ping utility able to determine the IP address associated with a domain name?
   3. What does the output of ping indicate?
   4. What does TTL mean, and what does it indicate in the ping output?
   5. What TCP/IP protocol does ping use, and what layer of the TCP/IP stack does this protocol operate?

- The ``traceroute command`` can trace the route from your device to another host on the Internet. Tracert uses the same echo requests and replies as the ping command but in a different way. Execute the command: ``traceroute cbc.ca`` and observe the output.
   1. Briefly explain how the traceroute command works.
   2. The first row in the traceroute output contains information about “hops.” Briefly explain what “hops” means in this context.
   3. If congestion is not an issue in the network, what is the most likely reason for rows containing stars and “Request timed out” in traceroute output?

- The ``nslookup command`` is used to query Domain Name System (DNS) servers to obtain mappings of Fully Qualified Domain Name (FQDN) to IP address or vice versa. Install nslookup by executing the following command: ``sudo apt install dnsutils``. When the installation completes, execute the following command: ``nslookup cs.mcgill.ca``
   1. In the nslookup output, what does the line that starts with “Server:” indicate?
   2. What port does the DNS protocol uses?
   3. What layer of the TCP/IP protocol stack does the DNS protocol operate?
- **Address Resolution Protocol (ARP)**: When a network device needs to send an IP packet to another node on the same network, it must determine the physical address, i.e., the MAC address of the destination node. The sender of the packet knows the IPaddress of the destination but needs to discover the MAC address of the destination.  The ARP protocol facilitates this process. The ``arp -a`` command displays your device's current ARP table. It’s a dynamic table that changes frequently. The table lists the IP addresses and the corresponding MAC addresses of nodes the device recently communicated with. Execute the command: 
``arp -a`` and observe the output.

    1. Briefly explain how the ARP protocol works.
    2. Briefly explain why the MAC address of the destination node is required when a device needs to send an IP packet to another node on the same network.
    3. Execute the command ``ping google.ca`` followed by “**arp -a**” and note the output of the commands.
    4. Does an entry containing the IP address of google.ca show up in your device’s ARP table? Why or why not?
    5. What layer of the TCP/IP stack does ARP operate?

# Grade and Submission
When you finish, shutdown the raspberry Pi OS with 

``sudo shutdown -h now``

- Then, disconnect the Raspberry Pi and connect the Keyboard, Mouse, Monitor, etc., to the computer lab.

- Return the video adapter to your teacher. Call your teacher to get a grade.

- If you don’t reconnect the Keyboard, Mouse, Monitor, etc., your mark will be **0** in this lab.

- If you don’t call your teacher to check, your mark will be **0** in this lab.

- When you complete the lab, please submit the Word document containing your answers to the questions to **Moodle** via the **Assignment Week 2 – Networking** link. 

- We will discuss the answers to the questions during the latter part of the lab.

Original document by C. Davis and V. Ponce  
Gui for alt configuration